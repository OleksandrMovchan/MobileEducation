﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;

namespace App1
{
    class SecondActivity : Activity
    {
        EditText result;
        Button btnBack;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SecondActivity);
        }

        protected override void OnStart()
        {
            base.OnStart();

            btnBack = FindViewById<Button>(Resource.Id.btnBack);
            btnBack.Click += GoBack;

            result = FindViewById<EditText>(Resource.Id.edt_result);
            result.Text = Container.Result;

        }

        private void GoBack (object sender, EventArgs e)
        {
            Container.Result = result.Text;
            Finish();
        }

    }
}