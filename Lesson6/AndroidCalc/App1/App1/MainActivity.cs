﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;

namespace App1
{
    [Activity(Label = "App1", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity 
    {
        Button btnOne;
        Button btnTwo;
        Button btnThree;
        Button btnFour;
        Button btnFive;
        Button btnSix;
        Button btnSeven;
        Button btnEight;
        Button btnNine;
        Button btnZero;

        Button btnPlus;
        Button btnMinus;
        Button btnMultiple;
        Button btnDivide;
        Button btnEquals;

        Button btnCancel;

        TextView txtResult;

        double numberOne = 0;
        double numberTwo = 0;
        string operation = "+";
        bool isCalculated = false;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
        }

        protected override void OnStart()
        {
            base.OnStart();

            btnOne = FindViewById<Button>(Resource.Id.btnOne);
            btnTwo = FindViewById<Button>(Resource.Id.btnTwo);
            btnThree = FindViewById<Button>(Resource.Id.btnThree);
            btnFour = FindViewById<Button>(Resource.Id.btnFour);
            btnFive = FindViewById<Button>(Resource.Id.btnFive);
            btnSix = FindViewById<Button>(Resource.Id.btnSix);
            btnSeven = FindViewById<Button>(Resource.Id.btnSeven);
            btnEight = FindViewById<Button>(Resource.Id.btnEight);
            btnNine = FindViewById<Button>(Resource.Id.btnNine);
            btnZero = FindViewById<Button>(Resource.Id.btnZero);

            btnPlus = FindViewById<Button>(Resource.Id.btnPlus);
            btnMinus = FindViewById<Button>(Resource.Id.btnMinus);
            btnMultiple = FindViewById<Button>(Resource.Id.btnMultiple);
            btnDivide = FindViewById<Button>(Resource.Id.btnDivide);
            btnEquals = FindViewById<Button>(Resource.Id.btnEquals);

            btnCancel = FindViewById<Button>(Resource.Id.btnCancel);

            txtResult = FindViewById<TextView>(Resource.Id.txtResult);

            btnOne.Click += NumberPressed;
            btnTwo.Click += NumberPressed;
            btnThree.Click += NumberPressed;
            btnFour.Click += NumberPressed;
            btnFive.Click += NumberPressed;
            btnSix.Click += NumberPressed;
            btnSeven.Click += NumberPressed;
            btnEight.Click += NumberPressed;
            btnNine.Click += NumberPressed;
            btnZero.Click += NumberPressed;

            btnPlus.Click += OperationPressed;
            btnMinus.Click += OperationPressed;
            btnMultiple.Click += OperationPressed;
            btnDivide.Click += OperationPressed;

            btnCancel.Click += CancelPressed;

            btnEquals.Click += EqualsPressed;

            if (Container.Result != null)
            {
                txtResult.Text = Container.Result;
            }
            else
            {
                txtResult.Text = "0";
            }

        }

        protected override void OnStop()
        {
            base.OnStop();
            //OnResume -- OnPause
            btnOne.Click -= NumberPressed;
            btnTwo.Click -= NumberPressed;
            btnThree.Click -= NumberPressed;
            btnFour.Click -= NumberPressed;
            btnFive.Click -= NumberPressed;
            btnSix.Click -= NumberPressed;
            btnSeven.Click -= NumberPressed;
            btnEight.Click -= NumberPressed;
            btnNine.Click -= NumberPressed;
            btnZero.Click -= NumberPressed;

            btnPlus.Click -= OperationPressed;
            btnMinus.Click -= OperationPressed;
            btnMultiple.Click -= OperationPressed;
            btnDivide.Click -= OperationPressed;

            btnCancel.Click -= CancelPressed;

            btnEquals.Click -= EqualsPressed;
        }

        private void NumberPressed(object sender, EventArgs e)
        {
            if (isCalculated)
            {
                txtResult.Text = "0";
                AddNumber(((Button)sender).Text);
                isCalculated = false;
            }
            else
            {
                AddNumber(((Button)sender).Text);
            }
        }

        private void AddNumber(string number)
        {
            if (txtResult.Text.Equals("0") || txtResult.Text.Equals("Infinity") || txtResult.Text.Equals("Not an operation"))
            {
                txtResult.Text = number;
            }
            else
            {
                txtResult.Text += number;
            }
        }

        private void OperationPressed(object sender, EventArgs e)
        {
            operation = ((Button)sender).Text;
            string valueNumberOne = txtResult.Text;
            numberOne = double.Parse(valueNumberOne);
            txtResult.Text = "0";
        }

        private void CancelPressed(object sender, EventArgs e)
        {
            numberOne = 0;
            numberTwo = 0;
            txtResult.Text = "0";
        }

        private void EqualsPressed(object sender, EventArgs e)
        {
            string valueNumberTwo = txtResult.Text;
            numberTwo = double.Parse(valueNumberTwo);
            string result = "";
            switch (operation)
            {
                case "+" :
                    result += string.Concat(numberOne + numberTwo);
                    break;
                case "-":
                    result += string.Concat(numberOne - numberTwo);
                    break;
                case "*":
                    result += string.Concat(numberOne * numberTwo);
                    break;
                case "/":
                    if (numberTwo != 0)
                    {
                        result += string.Concat(numberOne - numberTwo);
                    }
                    else
                    {
                        result = "Infinity";
                    }
                    break;
                default: result = "Not an operation";
                    break;
            }

            numberOne = 0;
            numberTwo = 0;

            Container.Result = result;

            Intent intent = new Intent(this, typeof(SecondActivity));
            StartActivity(intent);
        }
    }
}

