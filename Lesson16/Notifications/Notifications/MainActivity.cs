﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using Android.Support.V4.App;

namespace Notifications
{
    [Activity(Label = "Notifications", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var btnPush = FindViewById<Button>(Resource.Id.btn_push);

            btnPush.Click += PushNotification;

        }

        private void PushNotification(object sender, EventArgs e)
        {
            Intent notificationIntent = new Intent(this, typeof(MainActivity));
            PendingIntent contentIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.CancelCurrent);
            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

            builder.SetContentIntent(contentIntent).SetSmallIcon(Resource.Drawable.kitty)
                .SetContentTitle("Напоминание")
                .SetContentText("Покорми, пожалуйста, котенка")
                .SetTicker("Иначе нагажу в тапок")
                .AddAction(Resource.Drawable.kitty, "Open", contentIntent)
                .AddAction(Resource.Drawable.kitty, "Close", contentIntent);
                /*.SetAutoCancel(true)*/;
            Console.WriteLine("Notification!!!");

            Notification notification = builder.Build();

            var notificationManager =
               GetSystemService(NotificationService) as NotificationManager;
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);

            //NotificationManager notificationManager = (NotificationManager) GetSystemService(Context.NotificationService);
        }
    }
}

