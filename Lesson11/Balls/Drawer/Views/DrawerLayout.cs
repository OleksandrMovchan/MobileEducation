﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Drawer.Views
{
    [Register("Drawer.Views.DrawerLayout")]
    class DrawerLayout : View
    {
        private List<Paint> paintsCircles = new List<Paint>();
        private List<Path> pathsCircles = new List<Path>();
        private List<Ball> balls = new List<Ball>();
        private Path.Direction dir = Path.Direction.Cw;

        Timer timer;
        
        private int _counter = -1;
        private int _paintWidth = 20;

        private int _red = 255;
        private int _green = 255;
        private int _blue = 255;

        public int PaintWidth { get => _paintWidth; set => _paintWidth = value; }
        public int Red { get => _red; set => _red = value; }
        public int Green { get => _green; set => _green = value; }
        public int Blue { get => _blue; set => _blue = value; }

        public List<Paint> PaintsCircles { get => paintsCircles; set => paintsCircles = value; }
        public List<Path> PathsCircles { get => pathsCircles; set => pathsCircles = value; }
        internal List<Ball> Balls { get => balls; set => balls = value; }
        public Path.Direction Dir { get => dir; set => dir = value; }

        Task task;

        public DrawerLayout(Context context) : base(context)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected DrawerLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public void Redraw()
        {
            Invalidate();
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            CreateTools();

            timer = new Timer(25)
            {
                Enabled = true,
                AutoReset = true
            };

            timer.Elapsed += (o, e) =>
            {
                ApplyChanges();
                Invalidate();
            };
        }

        private void ApplyChanges()
        {
            for (int i = 0; i < Balls.Count; i++)
            {
                Balls[i].Move();
                PathsCircles[i].Reset();
                PathsCircles[i].AddCircle(Balls[i].X, Balls[i].Y, PaintsCircles[i].StrokeWidth, Dir);
            }
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            if (_counter > 0)
            {
                for (int i = 0; i < Balls.Count; i++)
                {
                    canvas.DrawPath(PathsCircles[i], PaintsCircles[i]);
                }
            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            float x = e.GetX();
            float y = e.GetY();

            switch (e.Action)
            {
                case MotionEventActions.Down:
                    Balls.Add(new Ball(x, y, this.Height, this.Width));
                    PathsCircles[Balls.Count-1].AddCircle(x, y, _paintWidth, Dir);
                    CreateTools();
                    break;
                case MotionEventActions.Up:
                    break;
                default: return false;
            }

            Invalidate();

            return true;
        }

        private void CreateTools()
        {
            PaintsCircles.Add(new Paint());
            PathsCircles.Add(new Path());

            _counter++;

            PaintsCircles[_counter].SetARGB(255, _red, _green, _blue);
            
            PaintsCircles[_counter].StrokeWidth = _paintWidth;

            PaintsCircles[_counter].AntiAlias = true;
            PaintsCircles[_counter].SetStyle(Paint.Style.Fill);

            
        }

        public void Clear()
        {
            PaintsCircles = new List<Paint>();
            PathsCircles = new List<Path>();
            Balls = new List<Ball>();
            _counter = -1;
            CreateTools();
            Invalidate();
        }
    }
}