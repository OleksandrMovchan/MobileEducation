﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Drawer.Views
{
    class Ball
    {
        private float _x;
        private float _y;
        private int _dx;
        private int _dy;

        private float _height;
        private float _width;

        public float X { get => _x; set => _x = value; }
        public float Y { get => _y; set => _y = value; }

        private Random random = new Random();

        public Ball(float x, float y, float height, float width)
        {
            _x = x;
            _y = y;
            _height = height;
            _width = width;

            _dx = GenerateBool(random.NextDouble()) ? 1 : -1;
            _dy = GenerateBool(random.NextDouble()) ? 1 : -1;
        }

        public void Move()
        {
            UpdateDirections();
            _x += _dx;
            _y += _dy;
        }

        private void UpdateDirections()
        {
            if (_x + _dx >= _width || _x + _dx <= 0)
            {
                _dx = -_dx;
            }
            if (_y + _dy >= _height || _y + _dy <= 0)
            {
                _dy = -_dy;
            }
        }

        private bool GenerateBool(double a)
        {
            if (a < 0.5)
            {
                return true;
            } 
            else
            {
                return false;
            }
        }
    }
}