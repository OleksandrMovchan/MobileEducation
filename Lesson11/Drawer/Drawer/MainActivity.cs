﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Runtime;
using Android.Util;
using System;
using Android.Graphics;
using static Android.Views.View;
using static Android.Widget.SeekBar;
using Drawer.Views;

namespace Drawer
{
    [Activity(Label = "Drawer", MainLauncher = true)]
    public class MainActivity : Activity, IOnSeekBarChangeListener
    {
        DrawerLayout drawerLayout;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            SeekBar seekBar = FindViewById<SeekBar>(Resource.Id.bar);
            seekBar.SetOnSeekBarChangeListener(this);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            var btnClear = FindViewById<Button>(Resource.Id.btn_clear);
            btnClear.Click += ClearField;

            FindColors();
        }

        private void FindColors()
        {
            var black = FindViewById<ColorLayout>(Resource.Id.col_black);
            var red = FindViewById<ColorLayout>(Resource.Id.col_red);
            var orange = FindViewById<ColorLayout>(Resource.Id.col_orange);
            var yellow = FindViewById<ColorLayout>(Resource.Id.col_yellow);
            var green = FindViewById<ColorLayout>(Resource.Id.col_green);
            var blue = FindViewById<ColorLayout>(Resource.Id.col_blue);
            var darkblue = FindViewById<ColorLayout>(Resource.Id.col_darkblue);
            var violet = FindViewById<ColorLayout>(Resource.Id.col_violet);

            black.Red = 0; black.Green = 0; black.Blue = 0;
            red.Red = 244; red.Green = 26; red.Blue = 4;
            orange.Red = 244; orange.Green = 114; orange.Blue = 4;
            yellow.Red = 241; yellow.Green = 244; yellow.Blue = 58;
            green.Red = 66; green.Green = 175; green.Blue = 7;
            blue.Red = 121; blue.Green = 207; blue.Blue = 247;
            darkblue.Red = 9; darkblue.Green = 5; darkblue.Blue = 132;
            violet.Red = 95; violet.Green = 6; violet.Blue = 150;

            black.Click += ChangeColor;
            red.Click += ChangeColor;
            orange.Click += ChangeColor;
            yellow.Click += ChangeColor;
            green.Click += ChangeColor;
            blue.Click += ChangeColor;
            darkblue.Click += ChangeColor;
            violet.Click += ChangeColor;
        }

        private void ChangeColor(object sender, EventArgs e)
        {
            drawerLayout.Red = ((ColorLayout)sender).Red;
            drawerLayout.Green = ((ColorLayout)sender).Green;
            drawerLayout.Blue = ((ColorLayout)sender).Blue;
        }

        private void ClearField(object sender, EventArgs e)
        {
            drawerLayout.Clear();
        }

        public void OnProgressChanged(SeekBar seekBar, int progress, bool fromUser)
        {
        }

        public void OnStartTrackingTouch(SeekBar seekBar)
        {
        }

        public void OnStopTrackingTouch(SeekBar seekBar)
        {
            drawerLayout.PaintWidth = (seekBar.Progress / 5);
        }


    }
}

