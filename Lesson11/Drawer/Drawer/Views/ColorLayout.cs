﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Drawer.Views
{
    [Register("Drawer.Views.ColorLayout")]
    class ColorLayout : LinearLayout
    {
        private int _red = 255;
        private int _green = 255;
        private int _blue = 255;
        
        public int Red { get => _red; set => _red = value; }
        public int Green { get => _green; set => _green = value; }
        public int Blue { get => _blue; set => _blue = value; }

        public ColorLayout(Context context) : base(context)
        {
        }

        public ColorLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public ColorLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public ColorLayout(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected ColorLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();
        }
    }
}