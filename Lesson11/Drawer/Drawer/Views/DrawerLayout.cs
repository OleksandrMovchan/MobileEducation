﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Drawer.Views
{
    [Register("Drawer.Views.DrawerLayout")]
    class DrawerLayout : View
    {
        List<Paint> paintsLines = new List<Paint>();
        List<Paint> paintsCircles = new List<Paint>();
        List<Path> pathsLines = new List<Path>();
        List<Path> pathsCircles = new List<Path>();

        private bool _wasMoved = false;
        private int _counter = -1;
        private int _paintWidth = 20;

        private int _red = 255;
        private int _green = 255;
        private int _blue = 255;

        public int PaintWidth { get => _paintWidth; set => _paintWidth = value; }
        public int Red { get => _red; set => _red = value; }
        public int Green { get => _green; set => _green = value; }
        public int Blue { get => _blue; set => _blue = value; }

        public DrawerLayout(Context context) : base(context)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public DrawerLayout(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected DrawerLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            CreateTools(0, 0);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            if (_counter != -1)
            {
                DrawAll(canvas);
            }
        }

        private void DrawAll(Canvas canvas)
        {
            for (int i = 0; i < _counter; i++)
            {
                canvas.DrawPath(pathsLines[i], paintsLines[i]);
                canvas.DrawPath(pathsCircles[i], paintsCircles[i]);
            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            Invalidate();
            float x = e.GetX();
            float y = e.GetY();
            
            switch (e.Action)
            {
                case MotionEventActions.Down :
                    pathsLines[_counter].MoveTo(x, y);
                    _wasMoved = false;
                    return true;
                case MotionEventActions.Move:
                    pathsLines[_counter].LineTo(x, y);
                    _wasMoved = true;
                    break;
                case MotionEventActions.Up:
                    if (!_wasMoved)
                    {
                        Path.Direction dir = Path.Direction.Cw;
                        pathsCircles[_counter].AddCircle(x, y, 20, dir);
                    }
                    break;
                default: return false;
            }
            CreateTools(x, y);

            return true;
        }

        private void CreateTools(float x, float y)
        {
            paintsLines.Add(new Paint());
            pathsLines.Add(new Path());

            paintsCircles.Add(new Paint());
            pathsCircles.Add(new Path());

            _counter++;

            paintsLines[_counter].SetARGB(255, _red, _green, _blue);
            paintsCircles[_counter].SetARGB(255, _red, _green, _blue);

            paintsLines[_counter].StrokeWidth = _paintWidth;
            paintsCircles[_counter].StrokeWidth = _paintWidth;

            pathsLines[_counter].MoveTo(x, y);

            paintsLines[_counter].AntiAlias = true;
            paintsLines[_counter].SetStyle(Paint.Style.Stroke);

            paintsCircles[_counter].AntiAlias = true;
            paintsCircles[_counter].SetStyle(Paint.Style.Fill);

            
        }

        public void Clear()
        {
            paintsLines = new List<Paint>();
            paintsCircles = new List<Paint>();
            pathsLines = new List<Path>();
            pathsCircles = new List<Path>();
            _counter = -1;
            CreateTools(0, 0);
            Invalidate();
        }
    }
}