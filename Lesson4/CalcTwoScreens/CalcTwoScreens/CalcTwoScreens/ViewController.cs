﻿using System;
using Foundation;
using UIKit;

namespace CalcTwoScreens
{
    public partial class ViewController : UIViewController
    {
        double firstNum = 0;
        double secondNum = 0;
        double result = 0;
        string operation = "+";
        bool calculated = false;

        public string resultValue { get; set; }

        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            if (resultValue == null) resultValue = "0";

        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);


            btnOne.TouchUpInside -= NumberPressed;
            btnTwo.TouchUpInside -= NumberPressed;
            btnThree.TouchUpInside -= NumberPressed;
            btnFour.TouchUpInside -= NumberPressed;
            btnFive.TouchUpInside -= NumberPressed;
            btnSix.TouchUpInside -= NumberPressed;
            btnSeven.TouchUpInside -= NumberPressed;
            btnEight.TouchUpInside -= NumberPressed;
            btnNine.TouchUpInside -= NumberPressed;
            btnZero.TouchUpInside -= NumberPressed;

            btnPlus.TouchUpInside -= OperationPressed;
            btnMinus.TouchUpInside -= OperationPressed;
            btnMultiple.TouchUpInside -= OperationPressed;
            btnDivide.TouchUpInside -= OperationPressed;

            btnCancel.TouchUpInside -= CancelPressed;

            btnEquals.TouchUpInside -= EqualsPressed;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            txtResult.Text = resultValue;

            btnOne.TouchUpInside += NumberPressed;
            btnTwo.TouchUpInside += NumberPressed;
            btnThree.TouchUpInside += NumberPressed;
            btnFour.TouchUpInside += NumberPressed;
            btnFive.TouchUpInside += NumberPressed;
            btnSix.TouchUpInside += NumberPressed;
            btnSeven.TouchUpInside += NumberPressed;
            btnEight.TouchUpInside += NumberPressed;
            btnNine.TouchUpInside += NumberPressed;
            btnZero.TouchUpInside += NumberPressed;

            btnPlus.TouchUpInside += OperationPressed;
            btnMinus.TouchUpInside += OperationPressed;
            btnMultiple.TouchUpInside += OperationPressed;
            btnDivide.TouchUpInside += OperationPressed;

            btnCancel.TouchUpInside += CancelPressed;

            btnEquals.TouchUpInside += EqualsPressed;


            if (CalculationResult.result != null)
            {
                txtResult.Text = CalculationResult.result;
            }
            else
            {
                txtResult.Text = "0";
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            //Release any cached data, images, etc that aren't in use.
        }

        private void NumberPressed(object sender, EventArgs eventArgs)
        {
            if (calculated)
            {
                txtResult.Text = "0";
                AddNumber(sender);
                calculated = false;
            }
            else
            {
                AddNumber(sender);
            }
        }

        private void AddNumber(object sender)
        {
            string currentValue = txtResult.Text;
            if (currentValue.Equals("0"))
            {
                txtResult.Text = ((UIButton)sender).Title(UIControlState.Normal);
            }
            else
            {
                txtResult.Text += ((UIButton)sender).Title(UIControlState.Normal);
            }
        }

        private void OperationPressed(object sender, EventArgs eventArgs)
        {
            operation = ((UIButton)sender).Title(UIControlState.Normal);
            firstNum = double.Parse(txtResult.Text);
            txtResult.Text = "0";
        }

        private void CancelPressed(object sender, EventArgs eventArgs)
        {
            firstNum = 0;
            secondNum = 0;
            txtResult.Text = "0";
            calculated = false;
        }

        private void EqualsPressed(object sender, EventArgs eventArgs)
        {
            secondNum = double.Parse(txtResult.Text);
            result = Calculate();
            calculated = true;
            txtResult.Text = string.Concat(result);
            CalculationResult.result = string.Concat(result);
            DismissViewController(false, null);
            NavigationRouter.GoToSecondView(this, true);

        }

        private double Calculate()
        {
            switch (operation)
            {
                case "+":
                    return firstNum + secondNum;
                case "-":
                    return firstNum - secondNum;
                case "*":
                    return firstNum * secondNum;
                case "/":
                    if (secondNum != 0)
                    {
                        return firstNum / secondNum;
                    }
                    else
                    {
                        return 0;
                    }
                default:
                    return 0;
            }
        }
    }
}
