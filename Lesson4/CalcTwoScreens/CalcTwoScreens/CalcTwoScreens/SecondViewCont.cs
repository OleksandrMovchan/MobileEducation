using Foundation;
using System;
using UIKit;

namespace CalcTwoScreens
{
    public partial class SecondViewCont : UIViewController
    {
        public SecondViewCont (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
            base.ViewDidLoad();
		}

		public override void ViewWillAppear(bool animated)
		{
            base.ViewWillAppear(animated);
		}

		public override void ViewDidAppear(bool animated)
		{
            base.ViewDidAppear(animated);

            if (CalculationResult.result != null) {
                edText.Text = CalculationResult.result;
            } else {
                edText.Text = "There are any values";
            }
		}

		partial void BtnGoBack_TouchUpInside(UIButton sender)
        {
            CalculationResult.result = edText.Text;
            DismissViewController(false, null);
            NavigationRouter.GoToFirstView(this, true);

        }
    }
}