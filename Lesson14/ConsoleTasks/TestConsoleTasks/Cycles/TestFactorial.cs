﻿using ConsoleTasks.Cycles;
using NUnit.Framework;
using System;

namespace TestConsoleTasks.Cycles
{
    [TestFixture]
    public class TestFactorial
    {
        [TestCase(0, ExpectedResult = 1)] //For zero
        [TestCase(1, ExpectedResult = 1)] //For one
        [TestCase(4, ExpectedResult = 24)] //For other positive
        public int Calculate_Test(int f)
        {
            return Factorial.Calculate(f);
        }

        [Test]
        public void Calculate_Negative()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Factorial.Calculate(-5);
            });
        }
    }
}
