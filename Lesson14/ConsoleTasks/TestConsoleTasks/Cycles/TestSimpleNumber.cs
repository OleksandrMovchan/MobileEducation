﻿using ConsoleTasks.Cycles;
using NUnit.Framework;
using System;

namespace TestConsoleTasks.Cycles
{
    [TestFixture]
    public class TestSimpleNumber
    {
        [TestCase(1, ExpectedResult = true)] //For one
        [TestCase(11, ExpectedResult = true)] //For prime
        [TestCase(8, ExpectedResult = false)] //For composite
        public bool IsPrime_Test(int number)
        {
            return PrimeNumber.IsPrime(number);
        }
        
        [TestCase(0)] //For zero
        [TestCase(-8)] //For negative
        public void IsPrime_Exception(int number)
        {
           Assert.Throws<ArgumentException>(() =>
           {
               PrimeNumber.IsPrime(number);
           });
        }
    }
}
