﻿using ConsoleTasks.Cycles;
using NUnit.Framework;
using System;

namespace TestConsoleTasks.Cycles
{
    [TestFixture]
    class TestEvenNumbers
    {
        [TestCase(0, ExpectedResult = 0)] //For zero
        [TestCase(1, ExpectedResult = 0)] //For one
        [TestCase(5, ExpectedResult = 2)] //For positive
        public int Calculate_TestQuantity(int num)
        {
            EvenNumbers.Calculate(num);
            return EvenNumbers.Quantity;
        }

        [TestCase(0, ExpectedResult = 0)] //For zero
        [TestCase(1, ExpectedResult = 0)] //For one
        [TestCase(5, ExpectedResult = 6)] //For positive
        public int Calculate_TestSum(int num)
        {
            EvenNumbers.Calculate(num);
            return EvenNumbers.Sum;
        }

        [Test]
        public void Calculate_TestQuantity_Null()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                EvenNumbers.Calculate(-5);
            });
        }
    }
}
