﻿using ConsoleTasks.Functions;
using NUnit.Framework;
using System;

namespace TestConsoleTasks.Function
{
    [TestFixture]
    public class TestNumbersAndLetters
    {
        [TestCase(1, ExpectedResult = "Monday")]
        [TestCase(2, ExpectedResult = "Tuesday")]
        [TestCase(3, ExpectedResult = "Wednesday")]
        [TestCase(4, ExpectedResult = "Thursday")]
        [TestCase(5, ExpectedResult = "Friday")]
        [TestCase(6, ExpectedResult = "Saturday")]
        [TestCase(7, ExpectedResult = "Sunday")]
        public string FindName_ValidDay(int n)
        {
            return DaysOfWeek.FindName(n);
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(8)]
        public void FindName_InvalidDay(int n)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                DaysOfWeek.FindName(n);
            });
        }
    }
}
