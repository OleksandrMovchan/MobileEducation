﻿using ConsoleTasks.Functions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleTasks.Function
{
    [TestFixture]
    public class TestDaysOfWeek
    {

        [TestCase(0, ExpectedResult = "zero")]
        [TestCase(1, ExpectedResult = "one")]
        [TestCase(9, ExpectedResult = "nine")]
        [TestCase(10, ExpectedResult = "ten")]
        [TestCase(19, ExpectedResult = "nineteen")]
        [TestCase(20, ExpectedResult = "twenty")]
        [TestCase(51, ExpectedResult = "fifty one")]
        [TestCase(99, ExpectedResult = "ninety nine")]
        [TestCase(100, ExpectedResult = "one hundred")]
        [TestCase(201, ExpectedResult = "two hundred one")]
        [TestCase(333, ExpectedResult = "three hundred thirty three")]
        [TestCase(999, ExpectedResult = "nine hundred ninety nine")]
        public string ToString_ValidDay(int n)
        {
            return NumbersAndLetters.ToString(n);
        }

        [TestCase(-1)]
        [TestCase(1000)]
        public void ToString_InvalidDay(int n)
        {
           Assert.Throws<ArgumentOutOfRangeException>(() =>
           {
               NumbersAndLetters.ToString(n);
           });
        }


    }
}
