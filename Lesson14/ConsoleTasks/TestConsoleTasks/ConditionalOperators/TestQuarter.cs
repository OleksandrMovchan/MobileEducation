﻿using ConsoleTasks.ConditionalOperators;
using NUnit.Framework;

namespace TestConsoleTasks.ConditionalOperators
{
    [TestFixture]
    class TestQuarter
    {
        [TestCase(4, 4, ExpectedResult = "I Quarter")]
        [TestCase(-4, 4, ExpectedResult = "II Quarter")]
        [TestCase(-4, -4, ExpectedResult = "III Quarter")]
        [TestCase(4, -4, ExpectedResult = "IV Quarter")]
        [TestCase(4, 0, ExpectedResult = "In X axe")]
        [TestCase(0, 4, ExpectedResult = "In Y axe")]
        [TestCase(0, 0, ExpectedResult = "In center")]
        public string DetermineQuart_Test(double x, double y)
        {
            return Quarter.DetermineQuart(x, y);
        }
    }
}
