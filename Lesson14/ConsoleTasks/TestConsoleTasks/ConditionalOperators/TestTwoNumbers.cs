﻿using ConsoleTasks.ConditionalOperators;
using NUnit.Framework;

namespace TestConsoleTasks.ConditionalOperators
{
    [TestFixture]
    public class TestTwoNumbers
    {
        [TestCase(4, 2, ExpectedResult = 8)] //A even B even
        [TestCase(4, 3, ExpectedResult = 12)] //A even B odd
        [TestCase(3, 2, ExpectedResult = 5)] //A odd B even
        [TestCase(3, 3, ExpectedResult = 6)] //A odd B odd
        [TestCase(0, 0, ExpectedResult = 0)] //A zero B zero
        public int DoOperation_Aeven_Beven(int a, int b)
        {
            
            return TwoNumbers.DoOperation(a, b);
        }
    }
}
