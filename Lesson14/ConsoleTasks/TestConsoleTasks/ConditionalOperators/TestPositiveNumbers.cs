﻿using ConsoleTasks.ConditionalOperators;
using NUnit.Framework;

namespace TestConsoleTasks.ConditionalOperators
{
    [TestFixture]
    public class TestPositiveNumbers
    {
        [TestCase(-1, -2, -3, ExpectedResult = 0)] //All negativer
        [TestCase(0, 0, 0, ExpectedResult = 0)] //All zeros
        [TestCase(-1, 2, 4, ExpectedResult = 6)] //Contains positives
        [TestCase(2, 4, 6, ExpectedResult = 12)] //All positives
        public int GetPositiveSum_Test(int a, int b, int c)
        {
            return PositiveNumbers.GetPositiveSum(a, b, c);
        }
    }
}
