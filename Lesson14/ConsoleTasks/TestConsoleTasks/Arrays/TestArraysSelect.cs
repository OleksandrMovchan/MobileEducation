﻿using ConsoleTasks.Arrays;
using NUnit.Framework;
using System;

namespace TestConsoleTasks.Arrays
{
    [TestFixture]
    public class TestArraysSelect
    { 
        private int[] _numbers = new int[]
        {
            2, 10, 8, -5, 0, 11, -8, -3, -1, 4
            // min => [6]=-8
            // max => [5]=11 
        };
        
        [Test]
        public void ElementMin_Test()
        {
            int expected = -8;
            int actual = ArraysSelect.ElementMin(_numbers);

            Assert.AreEqual(expected, actual, "Minimal element defined incorrectly");
        }

        [Test]
        public void ElementMin_SetNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                ArraysSelect.ElementMin(null);
            });
        }

        [Test]
        public void ElementMax_Test()
        {
            int expected = 11;
            int actual = ArraysSelect.ElementMax(_numbers);

            Assert.AreEqual(expected, actual, "Maximal element defined incorrectly");
        }

        [Test]
        public void ElementMax_SetNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                ArraysSelect.ElementMax(null);
            });
        }

        [Test]
        public void IndexMin_Test()
        {
            int expected = 6;
            int actual = ArraysSelect.IndexMin(_numbers);

            Assert.AreEqual(expected, actual, "Minimal element defined incorrectly");
        }

        [Test]
        public void IndexMin_SetNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                ArraysSelect.IndexMin(null);
            });
        }

        [Test]
        public void IndexMax_Test()
        {
            int expected = 5;
            int actual = ArraysSelect.IndexMax(_numbers);

            Assert.AreEqual(expected, actual, "Maximal element defined incorrectly");
        }

        [Test]
        public void IndexMax_SetNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                ArraysSelect.IndexMax(null);
            });
        }


    }
}
