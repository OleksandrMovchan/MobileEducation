﻿using System;

namespace ConsoleTasks.Cycles
{
    public static class Factorial
    {
        public static int Calculate(int number)
        {
            if (number < 0) throw new ArgumentException("Factorial could be calculated only for positive number");

            int result = 1;
            if (number == 0 || number == 1) return result;

            for (int i = 2; i <= number; i++)
            {
                result *= i;
            }

            return result;
        }
    }
}
