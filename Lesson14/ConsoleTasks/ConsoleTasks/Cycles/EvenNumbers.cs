﻿using System;

namespace ConsoleTasks.Cycles
{
    public static class EvenNumbers
    {
        private static int _sum = 0;
        private static int _quantity = 0;

        public static int Sum { get => _sum;  }
        public static int Quantity { get => _quantity; }

        public static void Calculate(int range)
        {
            if (range < 0)
            {
                throw new ArgumentException("Range should be bigger than zero");
            }

            if (range == 0 || range == 1)
            {
                _quantity = 0;
                _sum = 0;
                return;
            }

            int quantity = 0;
            int sum = 0;

            for (int i = 1; i <= range; i++)
            {
                if (i % 2 == 0)
                {
                    quantity++;
                    sum += i;
                }
            }

            _quantity = quantity;
            _sum = sum;
        }
    }
}
