﻿using System;

namespace ConsoleTasks.Cycles
{
    public static class PrimeNumber
    {
        public static bool IsPrime(int number)
        {
            if (number < 1) throw new ArgumentException("Simple number can be only 1 or bigger");

            if (number == 1 || number == 2) return true;

            for (int i = 2; i < number; i++)
            {
                if (number % i == 0) return false;
            }

            return true;
        }
    }
}
