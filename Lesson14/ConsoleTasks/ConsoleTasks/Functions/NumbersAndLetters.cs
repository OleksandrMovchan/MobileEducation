﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTasks.Functions
{
    public static class NumbersAndLetters
    {
        public static string ToString(int a)
        {
            string result = "zero";
            if (a < 0 | a > 999) throw new ArgumentOutOfRangeException();

            string[] p1 = { "", "one ", "two ", "three ", "four ", "five", "six ", "seven ", "eight ", "nine " };
            string[] p2 = { "ten ", "eleven ", "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen " };
            string[] p3 = { "", "", "twenty ", "thirty ", "fourty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety " };
            string[] p4 = { "", "one hundred ", "two hundred ", "three hundred ", "four hundred ", "five hundred ", "six hundred ", "seven hundred ", "eight hundred ", "nine hundred " };

            if (a / 100 != 0 && a % 100 == 0) result = p4[a / 100];
            if (a / 100 != 0 && a % 100 != 0)
            {
                int k = a % 100;
                if (k / 10 != 1) result = p4[a / 100] + p3[k / 10] + p1[k % 10];
                if (k / 10 == 1) result = p4[a / 100] + p2[k % 10];
            }
            if (a / 100 == 0 && a / 10 != 0 && a / 10 != 1) result = p3[a / 10] + p1[a % 10];
            if (a / 100 == 0 && a / 10 != 0 && a / 10 == 1) result = p2[a % 10];
            if (a >= 1 && a <= 9) result = p1[a];

            return result.Trim();

        }
    }
}
