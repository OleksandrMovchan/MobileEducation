﻿using System;

namespace ConsoleTasks.Arrays
{
    public static class ArraysSelect
    {
        public static int ElementMin(int[] numbers)
        {
            if (numbers == null)
            {
                throw new ArgumentNullException("Array is null");
            }

            return SearchMin(numbers, SelectType.ELEMENT);
        }

        public static int ElementMax(int[] numbers)
        {
            if (numbers == null)
            {
                throw new ArgumentNullException("Array is null");
            }

            return SearchMax(numbers, SelectType.ELEMENT);
        }

        public static int IndexMin(int[] numbers)
        {
            if (numbers == null)
            {
                throw new ArgumentNullException("Array is null");
            }

            return SearchMin(numbers, SelectType.INDEX);
        }

        public static int IndexMax(int[] numbers)
        {
            if (numbers == null)
            {
                throw new ArgumentNullException("Array is null");
            }

            return SearchMax(numbers, SelectType.INDEX);
        }

        private static int SearchMin(int[] numbers, SelectType type)
        {
            int index = 0;
            int element = numbers[0];

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] < element)
                {
                    element = numbers[i];
                    index = i;
                }
            }

            switch (type)
            {
                case SelectType.ELEMENT:
                    return element;
                case SelectType.INDEX:
                    return index;
                default: return 0;
            }
        }

        private static int SearchMax(int[] numbers, SelectType type)
        {
            int index = 0;
            int element = numbers[0];

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > element)
                {
                    element = numbers[i];
                    index = i;
                }
            }

            switch (type)
            {
                case SelectType.ELEMENT:
                    return element;
                case SelectType.INDEX:
                    return index;
                default: return 0;
            }
        }
    }
}
