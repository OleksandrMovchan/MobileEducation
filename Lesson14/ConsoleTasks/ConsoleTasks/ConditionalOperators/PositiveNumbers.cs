﻿namespace ConsoleTasks.ConditionalOperators
{
    public class PositiveNumbers
    {
        public static int GetPositiveSum(int a, int b, int c)
        {
            int sum = 0;
            int[] numbers = new int[3];
            numbers[0] = a;
            numbers[1] = b;
            numbers[2] = c;

            foreach (int number in numbers)
            {
                if (VerifyNumber(number)) sum += number;
            }
            return sum;
        }

        private static bool VerifyNumber(int num)
        {
            if (num > 0) return true;
            return false;
        }
    }
}