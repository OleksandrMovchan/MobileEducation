﻿namespace ConsoleTasks.ConditionalOperators
{
    public static class TwoNumbers
    {
        public static int DoOperation(int a, int b)
        {
            if (a % 2 == 0)
            {
                return a * b;
            }
            else
            {
                return a + b;
            }
        }
    }
}
