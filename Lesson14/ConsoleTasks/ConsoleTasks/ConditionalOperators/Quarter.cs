﻿namespace ConsoleTasks.ConditionalOperators
{
    public static class Quarter
    {

        public static string DetermineQuart(double x, double y)
        {
            if (x > 0)
            {
                return XBiggerThanZero(y);
            }
            else if (x < 0)
            {
                return XLowerThanZero(y);
            }
            else
            {
                if (y == 0)
                {
                    return "In center";
                }
                return "In Y axe";
            }
        }

        private static string XBiggerThanZero(double y)
        {
            if (y > 0)
            {
                return "I Quarter";
            }
            else if (y < 0)
            {
                return "IV Quarter";
            }
            else
            {
                return "In X axe";
            }
        }

        private static string XLowerThanZero(double y)
        {
            if (y > 0)
            {
                return "II Quarter";
            }
            else if (y < 0)
            {
                return "III Quarter";
            }
            else
            {
                return "In X axe";
            }
        }
    }
}