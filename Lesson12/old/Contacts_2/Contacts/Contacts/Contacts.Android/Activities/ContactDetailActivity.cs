﻿using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Activities
{
    [Activity(Label = "ContactDetailActivity")]
    public class ContactDetailActivity : Activity
    {
        private IDataWrapper _data;

        public const string IdKey = "idKey";


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DetailScreen);

            //filling data collection
            ContactsMockList _list = new ContactsMockList();
            _data = new DataWrapper(_list.Contacts);

            //filling userinfo
            int id = Intent.GetIntExtra(IdKey, -1);
            var infoView = FindViewById<InfoView>(Resource.Id.infoview);

            IRouter router = new AndroidRouter(this);
            IInteractorInfo interactor = new InteractorInfo(_data, id);
            IPresenterInfo presenter = new PresenterInfo(infoView, router, interactor);
            presenter.SetData();

            //button clict listener
            var btnBack = FindViewById<Button>(Resource.Id.info_btnback);
            btnBack.Click += delegate
            {
                presenter.BackClicked();
            };
        }
    }
}