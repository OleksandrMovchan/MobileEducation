﻿using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Adapter
{
    class ContactsViewHolderLeft : RecyclerView.ViewHolder, IContactsViewHolder
    {
        private LeftView _leftView;
        private IInteractorContact _interactor;

        public ContactsViewHolderLeft(View itemView, Context context, IDataWrapper data) : base(itemView)
        {
            _leftView = itemView.FindViewById<LeftView>(Resource.Id.hl_view);

            IRouter router = new AndroidRouter(context);
            _interactor = new InteractorContact(data);
            IPresenterContact presenter = new PresenterContact(_leftView);
            _interactor.Presenter = presenter;
            //_presenter.Interactor = _interactor;
            presenter.Router = router;
        }

        public void SetId(int id)
        {
            _interactor.SetId(id);
            //_interactor.Id = id;
            //_presenter.SetData();
            //_leftView.Presenter = _presenter;
        }
    }
}