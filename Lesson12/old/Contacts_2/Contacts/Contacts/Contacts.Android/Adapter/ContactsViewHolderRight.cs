﻿using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Droid.Adapter
{
    class ContactsViewHolderRight : RecyclerView.ViewHolder, IContactsViewHolder
    {
        private RightView _rightView;
        private IInteractorContact _interactor;

        public ContactsViewHolderRight(View itemView, Context context, IDataWrapper data) : base(itemView)
        {
            _rightView = itemView.FindViewById<RightView>(Resource.Id.hr_view);

            IRouter router = new AndroidRouter(context);
            _interactor = new InteractorContact(data);
            IPresenterContact presenter = new PresenterContact(_rightView);
            _interactor.Presenter = presenter;
            //_presenter.Interactor = _interactor;
            presenter.Router = router;
            
        }

        public void SetId(int id)
        {
            _interactor.SetId(id);
            //_interactor.Id = id;
            //_presenter.SetData();
            //_rightView.Presenter = _presenter;
        }
    }
}