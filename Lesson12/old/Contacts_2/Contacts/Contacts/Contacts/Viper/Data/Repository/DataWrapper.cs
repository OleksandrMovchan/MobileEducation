﻿using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;
using System.Collections.Generic;

namespace Contacts.Viper.Data.Repository
{
    public class DataWrapper : IDataWrapper
    {
        private List<IEntityContact> _data;

        public DataWrapper()
        {
            ContactsMockList list = new ContactsMockList();
            _data = list.Contacts;
        }

        public DataWrapper(List<IEntityContact> data)
        {
            _data = data;
        }

        public int GetCount()
        {
            return _data.Count;
        }

        public IEntityContact GetData(int id)
        {
            foreach (IEntityContact contact in _data)
            {
                if (contact.Id == id)
                {
                    return contact;
                }
            }
            return null;
        }
    }
}
