﻿using Contacts.Viper.ContactsList.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Viper.Data.Repository.Interface
{
    public interface IDataWrapper
    {
        //List<IEntityContact> Data { get; set; }
        IEntityContact GetData(int id);
        int GetCount();
    }
}
