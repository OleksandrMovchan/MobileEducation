﻿using System;
using System.Collections.Generic;

using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Viper.ContactsInfo
{
    public class InteractorInfo : IInteractorInfo
    {
        private IDataWrapper _data;
        private IPresenterInfo presenter;
        private int _id;

        public IPresenterInfo Presenter { get => presenter; set => presenter = value; }
        public int Id { get => _id; set => _id = value; }

        public InteractorInfo(IDataWrapper data, int id)
        {
            _data = data;
            Id = id;
        }

        public IEntityInfo GetData()
        {
            IEntityContact data = _data.GetData(Id);
            return ConvertData(data);
        }

        private IEntityInfo ConvertData(IEntityContact contact)
        {
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IEntityInfo user = new EntityInfo(name, number, photo);
            return user;
        }
    }
}