﻿using Contacts.Router;
using Contacts.Viper.ContactsInfo.Interfaces;

namespace Contacts.Viper.ContactsInfo
{
    public class PresenterInfo : IPresenterInfo
    {
        private IViewInfo _view;
        private IRouter _router;
        private IInteractorInfo _interactor;

        public PresenterInfo(IViewInfo view, IRouter router, IInteractorInfo interactor)
        {
            _view = view;
            _router = router;
            _interactor = interactor;
        }

        public void BackClicked()
        {
            _router.GoBack();
        }

        public void SetData()
        {
            IEntityInfo contact = _interactor.GetData();

            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }
    }
}