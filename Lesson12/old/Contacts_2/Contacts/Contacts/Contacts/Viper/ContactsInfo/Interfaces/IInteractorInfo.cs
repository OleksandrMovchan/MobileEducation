﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IInteractorInfo
    {
        int Id { get; set; }
        IPresenterInfo Presenter { get; set; }

        IEntityInfo GetData();
    }
}