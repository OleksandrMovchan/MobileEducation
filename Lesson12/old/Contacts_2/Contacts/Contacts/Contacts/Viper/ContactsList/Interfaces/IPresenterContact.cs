﻿using Contacts.Router;
using System;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IPresenterContact
    {
        event Action OnCardPress;
        event Action OnImagePress;

        IRouter Router { get; set; }
        //IInteractorContact Interactor { get; set; }

        void SetData(IUserEntityContact contact);
        void AvatarClicked();
        void CardClicked();
        void GoToContactInfo(int id);
        void GoToPhoto(int id);
    }
}