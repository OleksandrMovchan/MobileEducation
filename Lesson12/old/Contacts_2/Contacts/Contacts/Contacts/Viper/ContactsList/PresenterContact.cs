﻿using Contacts.Router;
using Contacts.Viper.ContactsList.Enums;
using Contacts.Viper.ContactsList.Interfaces;
using System;

namespace Contacts.Viper.ContactsList
{
    public class PresenterContact : IPresenterContact
    {
        //private IInteractorContact _interactor;
        private IRouter _router;
        private IViewContact _view;

        public event Action OnImagePress;
        public event Action OnCardPress;

        public IRouter Router { get => _router; set => _router = value; }
        //public IInteractorContact Interactor { get => _interactor; set => _interactor = value; }

        public PresenterContact(IViewContact view)
        {
            _view = view;
            _view.OnCardClick += CardClicked;
            _view.OnImageClick += AvatarClicked;

        }

        public void AvatarClicked()
        {
            OnImagePress?.Invoke();
            //Interactor.AvatarClicked();
        }

        public void CardClicked()
        {
            OnCardPress?.Invoke();
            //Interactor.CardClicked();
        }

        public void GoToContactInfo(int id)
        {
            Router.GoContactInfo(id);
        }

        public void GoToPhoto(int id)
        {
            Router.GoPhoto(id);
        }

        public void SetData(IUserEntityContact contact)
        {
            //IUserEntityContact contact = Interactor.GetData();

            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }
    }
}