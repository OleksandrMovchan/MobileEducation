﻿using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.ContactsList
{
    public class UserContact : IUserEntityContact
    {
        private int _id;
        private string _name;
        private string _number;
        private string _photo;

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public string Photo { get => _photo; set => _photo = value; }

        public UserContact(int id,  string name, string number, string photo)
        {
            _id = id;
            _name = name;
            _number = number;
            _photo = photo;
        }
    }
}