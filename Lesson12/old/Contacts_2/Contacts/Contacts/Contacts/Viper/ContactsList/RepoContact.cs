﻿using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.ContactsList
{
    public class RepoContact : IEntityContact
    {
        private string _name;
        private string _number;
        private string _photoMin;
        private string _photoFull;
        private int _id;

        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public int Id { get => _id; set => _id = value; }
        public string PhotoMin { get => _photoMin; set => _photoMin = value; }
        public string PhotoFull { get => _photoFull; set => _photoFull = value; }

        public RepoContact(int id, string name, string number, string photoMin, string photoFull)
        {
            _id = id;
            _name = name;
            _number = number;
            _photoMin = photoMin;
            _photoFull = photoFull;
        }
    }
}