﻿using Contacts.Viper.ContactsList.Enums;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Viper.ContactsList
{
    public class InteractorContact : IInteractorContact
    {
        private IDataWrapper _data;
        private IPresenterContact _presenter;
        private int _id;

        //public int Id { get => _id; set => _id = value; }

        public IPresenterContact Presenter {
            get
            {
                return _presenter;
            }
            set
            {
                if (_presenter != null)
                {
                    _presenter.OnCardPress -= CardClicked;
                    _presenter.OnImagePress -= AvatarClicked;

                }
                _presenter = value;

                _presenter.OnCardPress += CardClicked;
                _presenter.OnImagePress += AvatarClicked;
            }
        }

        public InteractorContact(IDataWrapper data)
        {
            _data = data;
        }

        //data только по id
        public IUserEntityContact GetData()
        {
            IEntityContact data = _data.GetData(_id);
            return ConvertData(data);
        }

        private IUserEntityContact ConvertData(IEntityContact contact)
        {
            int id = contact.Id;
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IUserEntityContact user = new UserContact(id, name, number, photo);
            return user;
        }

        //переход - в презентере. получение айдишника через интерактор 

        public void AvatarClicked()
        {
            VerificationResult result;
            // DO SOMETHING
            result = VerificationResult.ALLOWED;

            if (result == VerificationResult.ALLOWED)
            {
                _presenter.GoToPhoto(_id);
            }
        }

        public void CardClicked()
        {
            // DO SOMETHING
            VerificationResult result;
            // DO SOMETHING
            result = VerificationResult.ALLOWED;

            if (result == VerificationResult.ALLOWED)
            {
                _presenter.GoToContactInfo(_id);
            }
        }

        public void SetId(int id)
        {
            _id = id;
            IUserEntityContact contact = GetData();
            _presenter.SetData(contact);
        }
    }
}