﻿using Contacts.Router;
using Contacts.Viper.ContactsList.Enums;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IInteractorContact
    {
        //int Id { get; set; }
        IPresenterContact Presenter { get; set; }

        IUserEntityContact GetData();
        void AvatarClicked();
        void CardClicked();
        void SetId(int id);

    }
}