﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IEntityContact
    {
        int Id { get; }
        string PhotoMin { get; }
        string PhotoFull { get; }
        string Name { get; }
        string Number { get; }
    }
}