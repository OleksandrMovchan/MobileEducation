﻿using System;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IViewContact
    {
        event Action OnCardClick;
        event Action OnImageClick;

        void SetName(string name);
        void SetNumber(string number);
        void SetPhoto(string photo);
    }
}