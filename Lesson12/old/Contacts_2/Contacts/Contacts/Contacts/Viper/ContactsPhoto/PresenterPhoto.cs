﻿using System;
using Contacts.Router;
using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Viper.ContactsPhoto
{
    public class PresenterPhoto : IPresenterPhoto
    {
        private IViewPhoto _view;
        private IRouter _router;
        private IInteractorPhoto _interactor;

        public PresenterPhoto(IViewPhoto view, IRouter router , IInteractorPhoto interactor)
        {
            _view = view;
            _router = router;
            _interactor = interactor;
        }

        public void BackClicked()
        {
            _router.GoBack();
        }

        public void SetData()
        {
            IEntityPhoto contact = _interactor.GetData();
            _view.SetPhoto(contact.Photo);
        }
    }
}