﻿namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IEntityPhoto
    {
        int Id { get; }
        string Photo { get; }
    }
}