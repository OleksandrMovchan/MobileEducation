﻿using System;
using System.Collections.Generic;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository.Interface;

namespace Contacts.Viper.ContactsPhoto
{
    public class InteractorPhoto : IInteractorPhoto
    {
        private IDataWrapper _data;
        private int _id;

        public InteractorPhoto(IDataWrapper data, int id)
        {
            _data = data;
            _id = id;
        }

        public IEntityPhoto GetData()
        {
            IEntityContact data = _data.GetData(_id);
            return ConvertData(data);
        }

        private IEntityPhoto ConvertData(IEntityContact contact)
        {
            int id = contact.Id;
            string photo = contact.PhotoFull;

            IEntityPhoto user = new EntityPhoto(id, photo);
            return user;
        }
    }
}