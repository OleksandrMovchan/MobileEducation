﻿namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IPresenterPhoto
    {
        void BackClicked();
        void SetData();
    }
}