﻿namespace Contacts.Router
{
    public interface IRouter
    {
        void GoContactInfo(int id);
        void GoPhoto(int id);
        void GoBack();
    }
}