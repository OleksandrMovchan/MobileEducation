using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto;
using Contacts.Viper.ContactsPhoto.Interfaces;
using Contacts.Viper.Data.Repository;
using System;
using System.Collections.Generic;
using UIKit;

namespace Contacts.iOS
{
	public partial class ImageViewController : UIViewController, IViewPhoto
    {
        private int _id;

        public int Id { get => _id; set => _id = value; }

        public ImageViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ContactsMockList data = new ContactsMockList();
            List<IEntityContact> contacts = data.Contacts;

			IRouter router = new IOSRouter(NavigationController);
            IInteractorPhoto interactor = new InteractorPhoto(contacts, _id);
            IPresenterPhoto presenter = new PresenterPhoto(this, router, interactor);
			
			presenter.SetData();
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_photoCard.Image = image;
		}
	}
}