// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Contacts.iOS
{
    [Register ("ImageViewController")]
    partial class ImageViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _photoCard { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView _scrollView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_photoCard != null) {
                _photoCard.Dispose ();
                _photoCard = null;
            }

            if (_scrollView != null) {
                _scrollView.Dispose ();
                _scrollView = null;
            }
        }
    }
}