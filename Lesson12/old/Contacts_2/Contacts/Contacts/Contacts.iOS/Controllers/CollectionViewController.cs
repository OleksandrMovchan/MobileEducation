using Contacts.iOS.Cells;
using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Router;
using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace Contacts.iOS.Controllers
{
    public partial class CollectionViewController : UICollectionViewController
    {
		private ContactDelegate _contactDelegate;
        private ImageDelegate _imageDelegate;
        private UIImageView _imageView;

        public CollectionViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _imageView = new UIImageView(new CGRect(0, 0, View.Bounds.Width, View.Bounds.Height));
            _imageView.ContentMode = UIViewContentMode.ScaleToFill;

            _imageDelegate = new ImageDelegate();
			_imageDelegate.Tapped += DelegateImageTapped;

            _collectionView.RegisterNibForCell(ContactCell.Nib, ContactCell.Key);
            _contactDelegate = new ContactDelegate();
            _collectionView.Delegate = _contactDelegate;
			_collectionView.DataSource = new ContactDataSource(_imageDelegate, NavigationController);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
			_contactDelegate.CellTapped += DelegateCellTapped;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
			_contactDelegate.CellTapped -= DelegateCellTapped;
            _contactDelegate.CellTapped -= DelegateCellTapped;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        void DelegateCellTapped(NSIndexPath obj)
        {
            var cell = _collectionView.CellForItem(obj) as ContactCell;
            var presenter = cell.Presenter;

            presenter.CardClicked();
        }

        void DelegateImageTapped(NSIndexPath obj)
        {
            
            var cell = _collectionView.CellForItem(obj) as ContactCell;
            var presenter = cell.Presenter;

            presenter.AvatarClicked();
        }      
	}
}