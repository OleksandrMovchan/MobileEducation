using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace Contacts.iOS.Controllers
{
    public partial class InfoViewController : UIViewController, IViewInfo
    {
        private int _id;

        public int Id { get => _id; set => _id = value; }

        public InfoViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
                        
            ContactsMockList data = new ContactsMockList();
            List<IEntityContact> contacts = data.Contacts;

            IRouter router = new IOSRouter(NavigationController);
            IInteractorInfo interactor = new InteractorInfo(contacts, _id);
            IPresenterInfo presenter = new PresenterInfo(this, router, interactor);

            presenter.SetData();

			//BackPressed;
			_btnBack.TouchUpInside += delegate {
                presenter.BackClicked();
            };

		}

		private void BackPressed(object sender, EventArgs e)
		{
			NavigationController.DismissViewController(true, null);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
            
		}

		public void SetName(string name)
		{
			_infoName.Text = name;
		}

		public void SetNumber(string number)
		{
			_infoNumber.Text = number;
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_infoPhoto.Image = image;
		}
	}
}