﻿using System;
using Contacts.iOS.Controllers;
using Contacts.Router;
using Foundation;
using UIKit;

namespace Contacts.iOS.Router
{
	public class IOSRouter : IRouter
    {
		private UINavigationController _navController;

		public IOSRouter(UINavigationController navController)
		{
			_navController = navController;
		}

		public void GoBack()
		{
			_navController.PopViewController(true);
		}

		public void GoContactInfo(int id)
		{
            var dest = UIStoryboard.FromName("Main", NSBundle.MainBundle).InstantiateViewController("InfoViewController") as InfoViewController;
            dest.Id = id;
            _navController.PushViewController(dest, true);
		}

		public void GoPhoto(int id)
		{
            var dest = UIStoryboard.FromName("Main", NSBundle.MainBundle).InstantiateViewController("ImageViewController") as ImageViewController;
            dest.Id = id;
            _navController.PushViewController(dest, true);
		}
	}
}
