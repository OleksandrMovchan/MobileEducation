﻿using System;
using Contacts.iOS.Collections;
using Contacts.Viper.ContactsList.Interfaces;
using Foundation;
using UIKit;

namespace Contacts.iOS.Cells
{
	public partial class ContactCell : UICollectionViewCell, IViewContact
	{
		public static readonly NSString Key = new NSString("ContactCell");
		public static readonly UINib Nib;
		public int Id { get; set; }
        
		private IPresenterContact _presenter;
		private ImageDelegate _imageDelegate;
		private NSIndexPath _indexPath;
        
		public IPresenterContact Presenter { get => _presenter; set => _presenter = value; }
		public ImageDelegate ImageDelegate { get => _imageDelegate; set => _imageDelegate = value; }
		public NSIndexPath IndexPath { get => _indexPath; set => _indexPath = value; }

		static ContactCell()
		{
			Nib = UINib.FromName("ContactCell", NSBundle.MainBundle);
		}

		protected ContactCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			_photo.Layer.CornerRadius = 40;
			_photo.ClipsToBounds = true;


			_photo.UserInteractionEnabled = true;
			_photo.AddGestureRecognizer(new UITapGestureRecognizer(HandleAction));
		}

		private void HandleAction(UIGestureRecognizer recognizer)
		{

			if (recognizer.State == UIGestureRecognizerState.Ended)
			{
				_imageDelegate.OnTapped(_indexPath);
			}
		}

		public void AddDelegate(ImageDelegate @delegate)
		{
			_imageDelegate = @delegate;
		}

		public void SetName(string name)
		{
			_name.Text = name;
		}
        
		public void SetNumber(string number)
		{
			_number.Text = number;
		}

		public void SetPhoto(string photo)
		{
			UIImage image = ConvertImage.FromUrl(photo);
			_photo.Image = image;
		}
        
	}
}
