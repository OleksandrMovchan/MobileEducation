﻿using System;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using System.Net.Http;

namespace Contacts.iOS.Collections
{
    public static class ConvertImage
    {
		public static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                return UIImage.LoadFromData(data);
        }
    }
}
