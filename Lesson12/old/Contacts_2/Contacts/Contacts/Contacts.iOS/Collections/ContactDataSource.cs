﻿using System;
using System.Collections.Generic;
using Contacts.iOS.Cells;
using Contacts.iOS.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Foundation;
using UIKit;

namespace Contacts.iOS.Collections
{
	public class ContactDataSource : UICollectionViewDataSource
    {
		public List<IEntityContact> _contacts;
		private ImageDelegate _imageDelegate;
        UINavigationController _navigationController;

        public ContactDataSource(ImageDelegate imageDelegate, UINavigationController navigationController)
        {
			ContactsMockList data = new ContactsMockList();
            _contacts = data.Contacts;
			_imageDelegate = imageDelegate;
            _navigationController = navigationController;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.DequeueReusableCell(ContactCell.Key, indexPath) as ContactCell;
            int position = (int)indexPath.Item;

            IInteractorContact interactor = new InteractorContact(_contacts, position);
            IRouter router = new IOSRouter(_navigationController);

            IPresenterContact presenter = new PresenterContact(cell, interactor, router);
            
            presenter.SetData();

            cell.Presenter = presenter;
            cell.Id = position;
            cell.IndexPath = indexPath;
			cell.AddDelegate(_imageDelegate);

            return cell;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return _contacts == null ? 0 : _contacts.Count;
        }
	}
}
