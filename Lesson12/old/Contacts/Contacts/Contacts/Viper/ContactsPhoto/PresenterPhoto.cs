﻿using System;
using Contacts.Router;
using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Viper.ContactsPhoto
{
    public class PresenterPhoto : IPresenterPhoto
    {
        private IViewPhoto _view;
        private IRouter _router;

        public PresenterPhoto(IViewPhoto view, IRouter router)
        {
            _view = view;
            _router = router;
        }

        public void BackClicked()
        {
            _router.GoBack();
        }

        public void SetData(int id, IInteractorPhoto interactor)
        {
            IEntityPhoto contact = interactor.GetData(id);
            _view.SetPhoto(contact.Photo);
        }
    }
}