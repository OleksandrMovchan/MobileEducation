﻿namespace Contacts.Viper.ContactsPhoto.Interfaces
{
    public interface IInteractorPhoto
    {
        IEntityPhoto GetData(int id);
    }
}