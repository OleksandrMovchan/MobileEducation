﻿using System;
using System.Collections.Generic;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Viper.ContactsPhoto
{
    public class InteractorPhoto : IInteractorPhoto
    {
        private List<IRepoEntityContact> _data;

        public InteractorPhoto(List<IRepoEntityContact> data)
        {
            _data = data;
        }

        public IEntityPhoto GetData(int id)
        {
            foreach (IRepoEntityContact contact in _data)
            {
                if (contact.Id == id) return ConvertData(contact);
            }
            throw new NullReferenceException("");
        }

        private IEntityPhoto ConvertData(IRepoEntityContact contact)
        {
            int id = contact.Id;
            string photo = contact.PhotoFull;

            IEntityPhoto user = new EntityPhoto(id, photo);
            return user;
        }
    }
}