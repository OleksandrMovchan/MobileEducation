﻿using Contacts.Router;
using Contacts.Viper.ContactsInfo.Interfaces;

namespace Contacts.Viper.ContactsInfo
{
    public class PresenterInfo : IPresenterInfo
    {
        private IViewInfo _view;
        private IRouter _router;

        public PresenterInfo(IViewInfo view, IRouter router)
        {
            _view = view;
            _router = router;
        }

        public void BackClicked()
        {
            _router.GoBack();
        }

        public void SetData(int id, IInteractorInfo interactor)
        {
            IEntityInfo contact = interactor.GetData(id);

            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }
    }
}