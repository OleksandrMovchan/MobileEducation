﻿using Contacts.Viper.ContactsInfo.Interfaces;

namespace Contacts.Viper.ContactsInfo
{
    public class EntityInfo : IEntityInfo
    {
        string _name;
        string _number;
        string _photo;

        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        public string Photo { get => _photo; set => _photo = value; }

        public EntityInfo(string name, string number, string photo)
        {
            _name = name;
            _number = number;
            _photo = photo;
        }
    }
}