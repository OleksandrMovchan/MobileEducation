﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IPresenterInfo
    {
        void BackClicked();
        void SetData(int id, IInteractorInfo interactor);
    }
}