﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IInteractorInfo
    {
        IEntityInfo GetData(int id);
    }
}