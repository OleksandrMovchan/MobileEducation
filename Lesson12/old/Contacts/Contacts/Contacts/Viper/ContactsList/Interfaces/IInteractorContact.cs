﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IInteractorContact
    {
        IUserEntityContact GetData(int id);
        int AvatarClicked(int id);
        int CardClicked(int id);
    }
}