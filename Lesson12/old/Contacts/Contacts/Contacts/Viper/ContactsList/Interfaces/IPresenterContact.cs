﻿using Contacts.Router;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IPresenterContact
    {
        void SetData(IUserEntityContact contact);
        void AvatarClicked(int id, IRouter router, IInteractorContact interactor);
        void CardClicked(int id, IRouter router, IInteractorContact interactor);
    }
}