﻿using System;
using System.Collections.Generic;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.ContactsList
{
    public class InteractorContact : IInteractorContact
    {
        private List<IRepoEntityContact> _data;

        public InteractorContact(List<IRepoEntityContact> data)
        {
            _data = data;
        }

        public IUserEntityContact GetData(int id)
        {
            foreach (IRepoEntityContact contact in _data)
            {
                if (contact.Id == id) return ConvertData(contact);
            }
            throw new NullReferenceException("");
        }

        private IUserEntityContact ConvertData(IRepoEntityContact contact)
        {
            int id = contact.Id;
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IUserEntityContact user = new UserContact(id, name, number, photo);
            return user;
        }

        public int AvatarClicked(int id)
        {
            return 1;
        }

        public int CardClicked(int id)
        {
            return 1;
        }
    }
}