﻿using Contacts.Viper.ContactsPhoto.Interfaces;

namespace Contacts.Viper.ContactsPhoto
{
    public class EntityPhoto : IEntityPhoto
    {
        int _id;
        string _photo;

        public int Id { get => _id; set => _id = value; }
        public string Photo { get => _photo; set => _photo = value; }

        public EntityPhoto(int id, string photo)
        {
            _id = id;
            _photo = photo;
        }
    }
}