﻿using System;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.ContactsList
{
    public class PresenterContact : IPresenterContact
    {
        private IViewContact _view;

        public PresenterContact(IViewContact view)
        {
            _view = view;
        }

        public void AvatarClicked(int id, IRouter router, IInteractorContact interactor)
        {
            int result = interactor.AvatarClicked(id);
            if (result == 1)
            {
                router.GoPhoto(id);
            }
        }

        public void CardClicked(int id, IRouter router, IInteractorContact interactor)
        {
            int result = interactor.CardClicked(id);
            if (result == 1)
            {
                router.GoContactInfo(id);
            }
        }

		public void SetData(int id, IInteractorContact interactor)
        {
			IUserEntityContact contact = interactor.GetData(id);

            _view.SetName(contact.Name);
            _view.SetNumber(contact.Number);
            _view.SetPhoto(contact.Photo);
        }
    }
}