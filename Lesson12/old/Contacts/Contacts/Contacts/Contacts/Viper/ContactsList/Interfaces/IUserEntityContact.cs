﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IUserEntityContact : IEntityContact
    {
        int Id { get; }
        string Name { get; }
        string Number { get; }
        string Photo { get; }
    }
}