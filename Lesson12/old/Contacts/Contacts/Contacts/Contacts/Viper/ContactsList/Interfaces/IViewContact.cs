﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IViewContact
    {
        void SetName(string name);
        void SetNumber(string number);
        void SetPhoto(string photo);
    }
}