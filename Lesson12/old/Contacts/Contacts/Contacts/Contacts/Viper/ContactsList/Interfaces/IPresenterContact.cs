﻿using Contacts.Router;

namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IPresenterContact
    {
		void SetData(int id, IInteractorContact interactor);
        void AvatarClicked(int id, IRouter router, IInteractorContact interactor);
        void CardClicked(int id, IRouter router, IInteractorContact interactor);
    }
}