﻿namespace Contacts.Viper.ContactsList.Interfaces
{
    public interface IRepoEntityContact : IEntityContact
    {
        int Id { get; }
        string PhotoMin { get; }
        string PhotoFull { get; }
        string Name { get; }
        string Number { get; }
    }
}