﻿using System.Collections.Generic;

using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.Data.Repository
{
    public class ContactsMockList
    {
        public List<IRepoEntityContact> Contacts { get; set; }

        public ContactsMockList()
        {
            FillList();
        }

        private void FillList()
        {
            Contacts = new List<IRepoEntityContact>
            {
                new RepoContact(0, /*Resource.Drawable.Yoda_min,*/ "Master Yoda", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(1, /*Resource.Drawable.ObiWan_min,*/ "Obi-Wan Kenobi", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(2, /*Resource.Drawable.QuiGon_min,*/ "Qui-Gon Jinn", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(3, /*Resource.Drawable.MaceWindu_min,*/ "Mace Windu", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(4, /*Resource.Drawable.Luke_min,*/ "Luke Skywalker", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(5, /*Resource.Drawable.KiAdi_min,*/ "Ki Adi Mundi", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(6, /*Resource.Drawable.Maul_min,*/ "Darth Maul", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(7, /*Resource.Drawable.Dooku_min,*/ "Count Dooku", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(8, /*Resource.Drawable.Palpatine_min,*/ "Emperor Palpatine", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(9, /*Resource.Drawable.Vader_min,*/ "Darth Vader", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(10, /*Resource.Drawable.Krennik_min,*/ "Orson Krennik", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(11, /*Resource.Drawable.Tarkin_min,*/ "Grand Moff Tarkin", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(12, /*Resource.Drawable.Leia_min,*/ "Leia Organa", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(13, /*Resource.Drawable.Solo_min,*/ "Han Solo", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(14, /*Resource.Drawable.Chewbacca_min,*/ "Chewbacca", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(15, /*Resource.Drawable.JarJar_min,*/ "Jar Jar Binks", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(16, /*Resource.Drawable.Jabba_min,*/ "Jabba Hutt", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(17, /*Resource.Drawable.Rey_min,*/ "Rey", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(18, /*Resource.Drawable.Finn_min,*/ "Finn", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(19, /*Resource.Drawable.Poe_min,*/ "Poe Dameron", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(20, /*Resource.Drawable.Hux_min,*/ "General Hux", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(21, /*Resource.Drawable.KyloRen_min,*/ "Kylo Ren", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(22, /*Resource.Drawable.Phasma_min,*/ "Captain Phasma", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(23, /*Resource.Drawable.DJ_min,*/ "DJ", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg"),
                new RepoContact(24, /*Resource.Drawable.Snoke_min,*/ "Supreme Leader Snoke", "+380633213210", "https://image.ibb.co/c0ii1J/Chewbacca_min.jpg", "https://image.ibb.co/mEVkFd/Chewbacca.jpg")

            };
        }
    }
}