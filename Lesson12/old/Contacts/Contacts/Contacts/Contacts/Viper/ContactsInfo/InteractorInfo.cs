﻿using System;
using System.Collections.Generic;

using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Viper.ContactsInfo
{
    public class InteractorInfo : IInteractorInfo
    {
        private List<IRepoEntityContact> _data;

        public InteractorInfo(List<IRepoEntityContact> data)
        {
            _data = data;
        }

        public IEntityInfo GetData(int id)
        {
            foreach (IRepoEntityContact contact in _data)
            {
                if (contact.Id == id) return ConvertData(contact);
            }
            throw new NullReferenceException("");
        }

        private IEntityInfo ConvertData(IRepoEntityContact contact)
        {
            string name = contact.Name;
            string number = contact.Number;
            string photo = contact.PhotoMin;

            IEntityInfo user = new EntityInfo(name, number, photo);
            return user;
        }
    }
}