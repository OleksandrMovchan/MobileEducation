﻿namespace Contacts.Viper.ContactsInfo.Interfaces
{
    public interface IViewInfo
    {
        void SetName(string name);
        void SetNumber(string number);
        void SetPhoto(string photo);
    }
}