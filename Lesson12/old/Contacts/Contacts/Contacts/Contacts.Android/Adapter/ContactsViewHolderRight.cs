﻿using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Droid.Views;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Adapter
{
    class ContactsViewHolderRight : RecyclerView.ViewHolder, IContactsViewHolder
    {
        private RightView _rightView;
        private Context _context;
        private IPresenterContact _presenter;
        private IInteractorContact _interactor;

        public ContactsViewHolderRight(View itemView, Context context, List<IRepoEntityContact> data) : base(itemView)
        {
            _rightView = itemView.FindViewById<RightView>(Resource.Id.hr_view);
            _presenter = new PresenterContact(_rightView);
            _interactor = new InteractorContact(data);
            _context = context;  
        }

        public void SetData(int id)
        {
            IUserEntityContact contact = _interactor.GetData(id);
            _presenter.SetData(contact);
            _rightView.ClickListener(_presenter, _interactor, id, _context);
        }
    }
}