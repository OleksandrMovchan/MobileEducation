﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;

namespace Contacts.Droid.Adapter
{
    class ContactsAdapter : RecyclerView.Adapter
    {
        private List<IRepoEntityContact> _data = new List<IRepoEntityContact>();

        public ContactsAdapter()
        {
            ContactsMockList _list = new ContactsMockList();
            _data = _list.Contacts;
        }

        public override int ItemCount => _data.Count() + 1;

        public override int GetItemViewType(int position)
        {
            if (position == ItemCount - 1)
            {
                return 2;
            }
            else if (position % 2 == 0)
            {
                return 0;
            }
            else if (position % 2 == 1)
            {
                return 1;
            }

            return -10;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is SpinnerHolder)
            {
                SpinnerHolder spinnerHolder = holder as SpinnerHolder;
                return;
            }

            IContactsViewHolder contactsViewHolder;
            if (holder is ContactsViewHolderLeft)
            {
                contactsViewHolder = holder as ContactsViewHolderLeft;

            }
            else if (holder is ContactsViewHolderRight)
            {
                contactsViewHolder = holder as ContactsViewHolderRight;
            }
            else
            {
                throw new ArgumentException("wrong holder type");
            }
            
            contactsViewHolder.SetData(position);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView;

            if (viewType == 0)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_left, parent, false);
                ContactsViewHolderLeft contactsViewHolder = new ContactsViewHolderLeft(itemView, parent.Context, _data);
                return contactsViewHolder;
            }
            else if (viewType == 1)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_right, parent, false);
                ContactsViewHolderRight contactsViewHolder = new ContactsViewHolderRight(itemView, parent.Context, _data);
                return contactsViewHolder;
            }
            else if (viewType == 2)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_spinner, parent, false);
                SpinnerHolder contactsViewHolder = new SpinnerHolder(itemView);
                return contactsViewHolder;
            }
            else
            {
                throw new ArgumentException("wrong viewType");
            }
        }
    }
}