﻿using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Droid.Views;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Adapter
{
    class ContactsViewHolderLeft : RecyclerView.ViewHolder, IContactsViewHolder
    {
        private LeftView _leftView;
        private Context _context;
        private IPresenterContact _presenter;
        private IInteractorContact _interactor;
        
        public ContactsViewHolderLeft(View itemView, Context context, List<IRepoEntityContact> data) : base(itemView)
        {
            _leftView = itemView.FindViewById<LeftView>(Resource.Id.hl_view);
            _interactor = new InteractorContact(data);
            _presenter = new PresenterContact(_leftView);
            _context = context;
        }

        public void SetData(int id)
        {
            IUserEntityContact contact = _interactor.GetData(id);
            _presenter.SetData(contact);
            _leftView.ClickListener(_presenter, _interactor, id, _context);
        }
    }
}