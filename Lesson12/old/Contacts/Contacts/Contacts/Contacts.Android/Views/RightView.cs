﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.RightView")]
    class RightView : LinearLayout, IViewContact
    {
        private LayoutInflater _inflater;

        public RightView(Context context) : base(context)
        {
        }

        public RightView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected RightView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.RightView, this, true);

            var Card = FindViewById<CardView>(Resource.Id.rv_card);
            Card.Radius = 72;
        }

        internal void ClickListener(IPresenterContact presenter, IInteractorContact interactor, int id, Context context)
        {
            var card = FindViewById<CardView>(Resource.Id.rv_card);
            var photo = FindViewById<ImageView>(Resource.Id.rv_photo);

            IRouter router = new AndroidRouter(context);

            card.Click += delegate
            {
                presenter.CardClicked(id, router, interactor);
            };

            photo.Click += delegate
            {
                presenter.AvatarClicked(id, router, interactor);
            };
        }

        public void SetName(string name)
        {
            var nameField = FindViewById<TextView>(Resource.Id.rv_name);
            nameField.Text = name;
        }

        public void SetNumber(string number)
        {
            var numberField = FindViewById<TextView>(Resource.Id.rv_number);
            numberField.Text = number;
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.rv_photo);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }


    }
}