﻿using System.Net;
using Android.Graphics;

namespace Contacts.Droid.Views
{
    static class URLtoBitmapConverter
    {
        public static Bitmap ConvertImage(System.String url)
        {
            Bitmap imageBitmap = null;
            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;
        }
    }
}