using Contacts.iOS.Cells;
using Contacts.iOS.Collections;
using Contacts.iOS.Router;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Router;
using Foundation;
using System;
using UIKit;

namespace Contacts.iOS.Controllers
{
    public partial class CollectionViewController : UICollectionViewController
    {
		private ContactDelegate _contactDelegate;

        public CollectionViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			_collectionView.RegisterNibForCell(ContactCell.Nib, ContactCell.Key);
			_contactDelegate = new ContactDelegate();
			_collectionView.Delegate = _contactDelegate;
			_collectionView.DataSource = new ContactDataSource();
		}

		public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
			_contactDelegate.CellTapped += DelegateCellTapped;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
			_contactDelegate.CellTapped -= DelegateCellTapped;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

		void DelegateCellTapped(NSIndexPath obj)
        {
			var dest = UIStoryboard.FromName("Main", NSBundle.MainBundle).InstantiateViewController("InfoViewController");
            var cell = _collectionView.CellForItem(obj) as ContactCell;

			var interactor = cell.Interactor;
			var presenter = cell.Presenter;
			int id = cell.Id;
			IdContainer.Id = id;
			IRouter router = new IOSRouter(dest, NavigationController);

			presenter.CardClicked(id, router, interactor);
        }

	}
}