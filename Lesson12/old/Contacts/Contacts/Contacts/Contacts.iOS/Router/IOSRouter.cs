﻿using System;
using Contacts.Router;
using UIKit;

namespace Contacts.iOS.Router
{
	public class IOSRouter : IRouter
    {
		private UIViewController _destination;
		private UINavigationController _navController;

		public IOSRouter(UIViewController destination, UINavigationController navController)
		{
			_destination = destination;
			_navController = navController;
		}

		public void GoBack()
		{
			_navController.PopViewController(true);
		}

		public void GoContactInfo(int id)
		{
			_navController.PushViewController(_destination, true);
		}

		public void GoPhoto(int id)
		{
			throw new NotImplementedException();
		}
	}
}
