﻿using System;
using Contacts.Viper.ContactsList.Interfaces;
using Foundation;
using UIKit;

namespace Contacts.iOS.Cells
{
	public partial class ContactCell : UICollectionViewCell, IViewContact
	{
		public static readonly NSString Key = new NSString("ContactCell");
		public static readonly UINib Nib;

		public int Id { get; set; }
		private IInteractorContact _interactor;
		private IPresenterContact _presenter;

		public IInteractorContact Interactor { get => _interactor; set => _interactor = value; }
        public IPresenterContact Presenter { get => _presenter; set => _presenter = value; }

		static ContactCell()
		{
			Nib = UINib.FromName("ContactCell", NSBundle.MainBundle);
		}

		protected ContactCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public void SetName(string name)
		{
			_name.Text = name;
		}

		public void SetNumber(string number)
		{
			_number.Text = number;
		}

		public void SetPhoto(string photo)
		{
			//_photo.Image = UIImage.FromResource(null, photo);
		}
	}
}
