﻿using System;
using System.Collections.Generic;
using Contacts.iOS.Cells;
using Contacts.Viper.ContactsList;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;
using Foundation;
using UIKit;

namespace Contacts.iOS.Collections
{
	public class ContactDataSource : UICollectionViewDataSource
    {
		public List<IRepoEntityContact> _contacts;

        public ContactDataSource()
        {
			ContactsMockList data = new ContactsMockList();
			_contacts = data.Contacts;
        }

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell(ContactCell.Key, indexPath) as ContactCell;
			IInteractorContact interactor = new InteractorContact(_contacts);
			IPresenterContact presenter = new PresenterContact(cell);
			int position = (int)indexPath.Item;
			presenter.SetData(position, interactor);
			cell.Interactor = interactor;
			cell.Presenter = presenter;
			cell.Id = position;
            return cell;
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return _contacts == null ? 0 : _contacts.Count;
		}
	}
}
