﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace Contacts.iOS.Collections
{
	public class ContactDelegate : UICollectionViewDelegateFlowLayout
    {
		public event Action<NSIndexPath> CellTapped;

		public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
        {
            return new CGSize(collectionView.Frame.Width, 100);
        }

        public override nfloat GetMinimumLineSpacingForSection(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
        {
            return 10;
        }

		public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            CellTapped?.Invoke(indexPath);
        }
    }
}
