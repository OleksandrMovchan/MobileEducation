﻿using Android.App;
using Android.Content;
using Contacts.Droid.Activities;
using Contacts.Router;

namespace Contacts.Droid.Router
{
    class AndroidRouter : IRouter
    {
        private Context _ctx;

        public AndroidRouter(Context context)
        {
            _ctx = context;
        }

        public void GoBack()
        {
            (_ctx as Activity)?.OnBackPressed();
        }

        public void GoContactInfo(int id)
        {
            Intent intent = new Intent(_ctx, typeof(ContactDetailActivity));
            intent.PutExtra(ContactDetailActivity.IdKey, id);
            _ctx.StartActivity(intent);
        }

        public void GoPhoto(int id)
        {
            Intent intent = new Intent(_ctx, typeof(PhotoActivity));
            intent.PutExtra(PhotoActivity.IdKey, id);
            _ctx.StartActivity(intent);
            
        }
    }
}