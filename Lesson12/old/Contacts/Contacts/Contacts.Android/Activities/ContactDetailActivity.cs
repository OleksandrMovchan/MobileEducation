﻿using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Droid.Views;
using Contacts.Router;
using Contacts.Viper.ContactsInfo;
using Contacts.Viper.ContactsInfo.Interfaces;
using Contacts.Viper.ContactsList.Interfaces;
using Contacts.Viper.Data.Repository;

namespace Contacts.Droid.Activities
{
    [Activity(Label = "ContactDetailActivity")]
    public class ContactDetailActivity : Activity
    {
        private List<IRepoEntityContact> _data = new List<IRepoEntityContact>();

        public const string IdKey = "idKey";


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DetailScreen);

            //filling data collection
            ContactsMockList _list = new ContactsMockList();
            _data = _list.Contacts;

            //filling userinfo
            int id = Intent.GetIntExtra(IdKey, -1);
            var infoView = FindViewById<InfoView>(Resource.Id.infoview);
            IInteractorInfo interactor = new InteractorInfo(_data);
            IRouter router = new AndroidRouter(this);
            IPresenterInfo presenter = new PresenterInfo(infoView, router);
            presenter.SetData(id, interactor);

            //button clict listener
            var btnBack = FindViewById<Button>(Resource.Id.info_btnback);
            btnBack.Click += delegate
            {
                presenter.BackClicked();
            };
        }
    }
}