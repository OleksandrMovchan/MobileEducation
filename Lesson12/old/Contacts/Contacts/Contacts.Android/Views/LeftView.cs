﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Droid.Router;
using Contacts.Router;
using Contacts.Viper.ContactsList.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.LeftView")]
    class LeftView : LinearLayout, IViewContact
    {
        private LayoutInflater _inflater;

        public LeftView(Context context) : base(context)
        {
        }

        public LeftView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public LeftView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public LeftView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected LeftView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.LeftView, this, true);
            var card = FindViewById<CardView>(Resource.Id.lv_card);
            card.Radius = 72;
            
            
        }

        public void ClickListener(IPresenterContact presenter, IInteractorContact interactor, int id, Context context)
        {
            var card = FindViewById<CardView>(Resource.Id.lv_card);
            var photo = FindViewById<ImageView>(Resource.Id.lv_photo);

            IRouter router = new AndroidRouter(context);

            photo.Click += delegate
            {
                presenter.AvatarClicked(id, router, interactor);
            };

            card.Click += delegate
            {
                presenter.CardClicked(id, router, interactor);
            };
        }

        public void SetName(string name)
        {
            var nameField = FindViewById<TextView>(Resource.Id.lv_name);
            nameField.Text = name;
        }

        public void SetNumber(string number)
        {
            var numberField = FindViewById<TextView>(Resource.Id.lv_number);
            numberField.Text = number;
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.lv_photo);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }
    }
}