﻿using System;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Contacts.Viper.ContactsInfo.Interfaces;

namespace Contacts.Droid.Views
{
    [Register("Contacts.Views.InfoView")]
    class InfoView : LinearLayout, IViewInfo
    {
        private LayoutInflater _inflater;

        public InfoView(Context context) : base(context)
        {
        }

        public InfoView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public InfoView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public InfoView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected InfoView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.InfoView, this, true);
        }

        public void SetName(string name)
        {
            var nameField = FindViewById<TextView>(Resource.Id.info_name);
            Application.SynchronizationContext.Post(a =>
            {
                nameField.Text = name;
            }, null);
        }

        public void SetNumber(string number)
        {
            var numberField = FindViewById<TextView>(Resource.Id.info_number);
            (Context as Activity)?.RunOnUiThread(() =>
            {
                numberField.Text = number;
            });
        }

        public void SetPhoto(string photo)
        {
            var photoField = FindViewById<ImageView>(Resource.Id.info_photo);
            Bitmap image = URLtoBitmapConverter.ConvertImage(photo);
            photoField.SetImageBitmap(image);
        }
    }
}