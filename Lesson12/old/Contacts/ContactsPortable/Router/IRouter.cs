﻿namespace ContactsPortable.Router
{
    public interface IRouter
    {
        void GoContactInfo(int id);
        void GoBack();
    }
}