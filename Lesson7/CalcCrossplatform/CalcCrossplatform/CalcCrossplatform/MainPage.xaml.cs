﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CalcCrossplatform
{
	public partial class MainPage : ContentPage
	{
        //Label txtResult;
      
        public MainPage()
		{
			InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //txtResult = this.FindByName<Label>("txtResult");
            /*
            Button btnOne = this.FindByName<Button>("btnOne");
            Button btnTwo = this.FindByName<Button>("btnTwo");
            Button btnThree = this.FindByName<Button>("btnThree");
            Button btnFour = this.FindByName<Button>("btnFour");
            Button btnFive = this.FindByName<Button>("btnFive");
            Button btnSix = this.FindByName<Button>("btnSix");
            Button btnSeven = this.FindByName<Button>("btnSeven");
            Button btnEight = this.FindByName<Button>("btnEight");
            Button btnNine = this.FindByName<Button>("btnNine");
            Button btnZero = this.FindByName<Button>("btnZero");

            Button btnPlus = this.FindByName<Button>("btnPlus");
            Button btnMinus = this.FindByName<Button>("btnMinus");
            Button btnMultiple = this.FindByName<Button>("btnMultiple");
            Button btnDivide = this.FindByName<Button>("btnDivide");
            Button btnEquals = this.FindByName<Button>("btnEquals");
            Button btnCancel = this.FindByName<Button>("btnCancel");
            */

            btnOne.Clicked += NumberPressed;
            btnTwo.Clicked += NumberPressed;
            btnThree.Clicked += NumberPressed;
            btnFour.Clicked += NumberPressed;
            btnFive.Clicked += NumberPressed;
            btnSix.Clicked += NumberPressed;
            btnSeven.Clicked += NumberPressed;
            btnEight.Clicked += NumberPressed;
            btnNine.Clicked += NumberPressed;
            btnZero.Clicked += NumberPressed;

            btnPlus.Clicked += OperationPressed;
            btnMinus.Clicked += OperationPressed;
            btnMultiple.Clicked += OperationPressed;
            btnDivide.Clicked += OperationPressed;

            btnCancel.Clicked += CancelPressed;

            btnEquals.Clicked += EqualsPressed;

            txtResult.Text = "0";
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            btnOne.Clicked -= NumberPressed;
            btnTwo.Clicked -= NumberPressed;
            btnThree.Clicked -= NumberPressed;
            btnFour.Clicked -= NumberPressed;
            btnFive.Clicked -= NumberPressed;
            btnSix.Clicked -= NumberPressed;
            btnSeven.Clicked -= NumberPressed;
            btnEight.Clicked -= NumberPressed;
            btnNine.Clicked -= NumberPressed;
            btnZero.Clicked -= NumberPressed;

            btnPlus.Clicked -= OperationPressed;
            btnMinus.Clicked -= OperationPressed;
            btnMultiple.Clicked -= OperationPressed;
            btnDivide.Clicked -= OperationPressed;

            btnCancel.Clicked -= CancelPressed;

            btnEquals.Clicked -= EqualsPressed;
        }

        private void NumberPressed(object sender, EventArgs e)
        {
            string num = ((Button)sender).Text;
            Calculating.NumberPressed(num);
            txtResult.Text = Calculating.result;
        }

        private void OperationPressed(object sender, EventArgs e)
        {
            string oper = ((Button)sender).Text;
            Calculating.OperationPressed(oper);
            txtResult.Text = Calculating.result;
        }

        private void CancelPressed(object sender, EventArgs e)
        {
            Calculating.CancelPressed();
            txtResult.Text = Calculating.result;
        }

        private async void EqualsPressed(object sender, EventArgs e)
        {
            Calculating.EqualsPressed();

            //SecondPage secondPage = new SecondPage();
            await Navigation.PushAsync(new SecondPage());
        }
    }
}
