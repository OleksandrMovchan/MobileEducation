﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CalcCrossplatform
{

    public partial class SecondPage : ContentPage
    {
        TextCell edTxt;

        public SecondPage()
        {
     
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            /*
            edTxt = this.FindByName<TextCell>("edTxt");
            var btnBack = this.FindByName<Button>("btnBack");
            */

            btnBack.Clicked += BackPressed;

            edTxt.Text = Calculating.result;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            var btnBack = this.FindByName<Button>("btnBack");
            btnBack.Clicked -= BackPressed;
        }

        private async void BackPressed(object sender, EventArgs e)
        {
            Calculating.result = edTxt.Text;
            
            await Navigation.PopAsync();
        }
    }
}
