﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CalcCrossplatform
{
    class Calculating
    {
        public static double num1 = 0;
        public static double num2 = 0;
        public static string operation = "+";
        public static string result = "0";
        public static bool calculated = false;

        public static void NumberPressed(string num)
        {
            if (calculated)
            {
                result = "0";
                AddNumber(num);
                calculated = false;
            }
            else
            {
                AddNumber(num);
            }
        }

        private static void AddNumber(string number)
        {
            if (result.Equals("0") || result.Equals("Infinity") || result.Equals("Not an operation"))
            {
                result = number;
            }
            else
            {
                result += number;
            }
        }

        public static void OperationPressed(string oper)
        {
            operation = oper;
            string valueNumberOne = result;
            num1 = double.Parse(valueNumberOne);
            result = "0";
        }

        public static void CancelPressed()
        {
            num1 = 0;
            num2 = 0;
            result = "0";
        }

        public static void EqualsPressed()
        {
            string valueNumberTwo = result;
            num2 = double.Parse(valueNumberTwo);
            result = "";
            switch (operation)
            {
                case "+":
                    result += string.Concat(num1 + num2);
                    break;
                case "-":
                    result += string.Concat(num1 - num2);
                    break;
                case "*":
                    result += string.Concat(num1 * num2);
                    break;
                case "/":
                    if (num2 != 0)
                    {
                        result += string.Concat(num1 - num2);
                    }
                    else
                    {
                        result = "Infinity";
                    }
                    break;
                default:
                    result = "Not an operation";
                    break;
            }

            num1 = 0;
            num2 = 0;

            /*
            Intent intent = new Intent(this, typeof(SecondActivity));
            StartActivity(intent);
            */
        }
    }
}
