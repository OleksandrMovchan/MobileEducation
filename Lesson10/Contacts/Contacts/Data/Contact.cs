﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Contacts.Data;

namespace Contacts
{
    class Contact
    {
        private int _img;
        private string _name;
        private string _number;
        private Side _side;

        public int Img { get => _img; set => _img = value; }
        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        internal Side Side { get => _side; set => _side = value; }

        public Contact(int img, string name, string number, Side side)
        {
            _img = img;
            _name = name;
            _number = number;
            _side = side;
        }
    }
}