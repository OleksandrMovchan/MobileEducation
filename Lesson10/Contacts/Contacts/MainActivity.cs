﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Contacts.Data;
using Android.Views;

namespace Contacts
{
    [Activity(Label = "Contacts", MainLauncher = true)]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            /*

                View vRep = getLayoutInflater().inflate(R.layout.tab_republic, null);
                View vEmp = getLayoutInflater().inflate(R.layout.tab_empire, null);
                View vReb = getLayoutInflater().inflate(R.layout.tab_rebels, null);
                View vFO = getLayoutInflater().inflate(R.layout.tab_firstorder, null);

                tabSpec = tabHost.newTabSpec("tag1");
                tabSpec.setIndicator(vRep);
                tabSpec.setContent(R.id.tab_one);
                tabHost.addTab(tabSpec);

                tabSpec = tabHost.newTabSpec("tag2");
                tabSpec.setIndicator(vEmp);
                tabSpec.setContent(R.id.tab_two);
                tabHost.addTab(tabSpec);

                tabSpec = tabHost.newTabSpec("tag3");
                tabSpec.setIndicator(vReb);
                tabSpec.setContent(R.id.tab_three);
                tabHost.addTab(tabSpec);

                tabSpec = tabHost.newTabSpec("tag4");
                tabSpec.setIndicator(vFO);
                tabSpec.setContent(R.id.tab_four);
                tabHost.addTab(tabSpec);

                tabHost.setCurrentTabByTag("tag1");
            */

            TabHost tabHost = FindViewById<TabHost>(Android.Resource.Id.TabHost);
            tabHost.Setup();

            TabHost.TabSpec tabSpec;

            View vRep = LayoutInflater.Inflate(Resource.Layout.tab_republic, null);
            View vEmp = LayoutInflater.Inflate(Resource.Layout.tab_empire, null);
            View vReb = LayoutInflater.Inflate(Resource.Layout.tab_rebels, null);
            View vFO = LayoutInflater.Inflate(Resource.Layout.tab_firstorder, null);

            tabSpec = tabHost.NewTabSpec("tag1");
            tabSpec.SetIndicator(vRep);
            tabSpec.SetContent(Resource.Id.tab_one);
            tabHost.AddTab(tabSpec);

            tabSpec = tabHost.NewTabSpec("tag2");
            tabSpec.SetIndicator(vEmp);
            tabSpec.SetContent(Resource.Id.tab_two);
            tabHost.AddTab(tabSpec);

            tabSpec = tabHost.NewTabSpec("tag3");
            tabSpec.SetIndicator(vReb);
            tabSpec.SetContent(Resource.Id.tab_three);
            tabHost.AddTab(tabSpec);

            tabSpec = tabHost.NewTabSpec("tag4");
            tabSpec.SetIndicator(vFO);
            tabSpec.SetContent(Resource.Id.tab_four);
            tabHost.AddTab(tabSpec);

            tabHost.SetCurrentTabByTag("tag1");

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

            RecyclerView recyclerViewRepublic = FindViewById<RecyclerView>(Resource.Id.contacts_list_rep);
            recyclerViewRepublic.SetLayoutManager(layoutManager);
            layoutManager = new LinearLayoutManager(this);

            RecyclerView recyclerViewEmpire = FindViewById<RecyclerView>(Resource.Id.contacts_list_emp);
            recyclerViewEmpire.SetLayoutManager(layoutManager);
            layoutManager = new LinearLayoutManager(this);

            RecyclerView recyclerViewRebels = FindViewById<RecyclerView>(Resource.Id.contacts_list_reb);
            recyclerViewRebels.SetLayoutManager(layoutManager);
            layoutManager = new LinearLayoutManager(this);

            RecyclerView recyclerViewFirstOrder = FindViewById<RecyclerView>(Resource.Id.contacts_list_fo);
            recyclerViewFirstOrder.SetLayoutManager(layoutManager);
            layoutManager = new LinearLayoutManager(this);

            List<Contact> _mock = new ContactsList().Contacts;

            ContactsAdapter contactsAdapterRepublic = new ContactsAdapter(_mock, Side.REPUBLIC);
            ContactsAdapter contactsAdapterEmpire = new ContactsAdapter(_mock, Side.EMPIRE);
            ContactsAdapter contactsAdapterRebels = new ContactsAdapter(_mock, Side.REBELS);
            ContactsAdapter contactsAdapterFirstOrder = new ContactsAdapter(_mock, Side.FIRST_ORDER);

            recyclerViewRepublic.SetAdapter(contactsAdapterRepublic);
            recyclerViewEmpire.SetAdapter(contactsAdapterEmpire);
            recyclerViewRebels.SetAdapter(contactsAdapterRebels);
            recyclerViewFirstOrder.SetAdapter(contactsAdapterFirstOrder);
        }
    }
}

