﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Contacts.Views;

namespace Contacts
{
    class ContactsViewHolderRight : RecyclerView.ViewHolder, ContactsViewHolder
    {
        private RightView rightView;

        public ContactsViewHolderRight(View itemView) : base(itemView)
        {
            rightView = itemView.FindViewById<RightView>(Resource.Id.hr_view);
        }

        public void SetData(Contact contact)
        {
            rightView.SetData(contact);
        }
    }
}