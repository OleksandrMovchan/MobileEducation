package com.example.android.lesson3;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnOne;
    Button btnTwo;
    Button btnThree;
    Button btnFour;
    Button btnFive;
    Button btnSix;
    Button btnSeven;
    Button btnEight;
    Button btnNine;
    Button btnZero;

    Button btnPlus;
    Button btnMinus;
    Button btnMultiple;
    Button btnDivide;
    Button btnEquals;

    Button btnCancel;

    TextView txtResult;

    double numberOne = 0;
    double numberTwo = 0;
    String operation = "+";
    boolean isCalculated = false;

    private final int RESULT_STR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        btnOne = (Button) findViewById(R.id.btnOne);
        btnTwo = (Button) findViewById(R.id.btnTwo);
        btnThree = (Button) findViewById(R.id.btnThree);
        btnFour = (Button) findViewById(R.id.btnFour);
        btnFive = (Button) findViewById(R.id.btnFive);
        btnSix = (Button) findViewById(R.id.btnSix);
        btnSeven = (Button) findViewById(R.id.btnSeven);
        btnEight = (Button) findViewById(R.id.btnEight);
        btnNine = (Button) findViewById(R.id.btnNine);
        btnZero = (Button) findViewById(R.id.btnZero);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnMultiple = (Button) findViewById(R.id.btnMultiple);
        btnDivide = (Button) findViewById(R.id.btnDivide);
        btnEquals = (Button) findViewById(R.id.btnEquals);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        txtResult = (TextView) findViewById(R.id.txtResult);

        btnOne.setOnClickListener(this);
        btnTwo.setOnClickListener(this);
        btnThree.setOnClickListener(this);
        btnFour.setOnClickListener(this);
        btnFive.setOnClickListener(this);
        btnSix.setOnClickListener(this);
        btnSeven.setOnClickListener(this);
        btnEight.setOnClickListener(this);
        btnNine.setOnClickListener(this);
        btnZero.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnMultiple.setOnClickListener(this);
        btnDivide.setOnClickListener(this);
        btnEquals.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (
                v.getId() == R.id.btnOne || v.getId() == R.id.btnTwo || v.getId() == R.id.btnThree ||
                        v.getId() == R.id.btnFour || v.getId() == R.id.btnFive || v.getId() == R.id.btnSix ||
                        v.getId() == R.id.btnSeven || v.getId() == R.id.btnEight || v.getId() == R.id.btnNine ||
                        v.getId() == R.id.btnZero
                ) {
            numberButtonPressed(v);
        } else if (
                v.getId() == R.id.btnPlus || v.getId() == R.id.btnMinus ||
                        v.getId() == R.id.btnMultiple || v.getId() == R.id.btnDivide
                ) {
            operationButtonPressed(v);
        } else if (v.getId() == R.id.btnEquals) {
            equalsButtonPressed(v);
        } else if (v.getId() == R.id.btnCancel) {
            cancelButtonPressed(v);
        }
    }

    private void numberButtonPressed(View v) {
        if (isCalculated) {
            txtResult.setText("0");
            addNumber(v);
            isCalculated = false;
        } else {
            addNumber(v);
        }
    }

    private void addNumber(View v) {
        Button pressedBtn = (Button) findViewById(v.getId());
        if (txtResult.getText().toString().equals("0")) {
            txtResult.setText(pressedBtn.getText().toString());
        } else {
            txtResult.append(pressedBtn.getText().toString());
        }
    }

    private void operationButtonPressed(View v) {
        Button pressedBtn = (Button) findViewById(v.getId());
        operation = pressedBtn.getText().toString();
        String sNumberOne = txtResult.getText().toString();
        numberOne = Double.parseDouble(sNumberOne);
        txtResult.setText("0");
    }

    private void cancelButtonPressed(View v) {
        numberOne = 0;
        numberTwo = 0;
        txtResult.setText("0");
        isCalculated = false;
    }

    @SuppressLint("RestrictedApi")
    private void equalsButtonPressed(View v) {
        String sNumberTwo = txtResult.getText().toString();
        numberTwo = Double.parseDouble(sNumberTwo);
        double result;
        switch (operation) {
            case "+":
                result = numberOne + numberTwo;
                break;
            case "-":
                result = numberOne - numberTwo;
                break;
            case "*":
                result = numberOne * numberTwo;
                break;
            case "/":
                if (numberTwo != 0) result = numberOne / numberTwo;
                else result = 0;
                break;
            default:
                result = 12345;
        }
        numberOne = 0;
        numberTwo = 0;

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("result", String.valueOf(result));
        startActivityForResult(intent,  0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            txtResult.setText(data.getStringExtra("result_calc"));
            isCalculated = true;
        }
    }
}
