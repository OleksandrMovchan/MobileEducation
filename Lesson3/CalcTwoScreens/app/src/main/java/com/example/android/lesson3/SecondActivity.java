package com.example.android.lesson3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener{

    EditText result;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        result = (EditText) findViewById(R.id.edt_result);
        Intent resultIntent = getIntent();
        String value = resultIntent.getStringExtra("result");
        result.setText(value);

        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("result_calc", result.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
