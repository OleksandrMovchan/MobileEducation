﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using System.Collections.Generic;

namespace Contacts
{
    [Activity(Label = "Contacts", MainLauncher = true)]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.contacts_list);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);

            List<Contact> _mock = new ContactsList().Contacts;

            ContactsAdapter contactsAdapter = new ContactsAdapter(_mock);
            recyclerView.SetAdapter(contactsAdapter);

        }
    }
}

