﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Contacts.Views
{
    [Register("Contacts.Views.RightView")]
    class RightView : LinearLayout, IView
    {
        private LayoutInflater _inflater;

        public RightView(Context context) : base(context)
        {
        }

        public RightView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public RightView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected RightView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnFinishInflate()
        {
            base.OnFinishInflate();

            _inflater = LayoutInflater.From(Context);
            _inflater.Inflate(Resource.Layout.RightView, this, true);
        }


        public void SetData(Contact contact)
        {
            var name = FindViewById<TextView>(Resource.Id.rv_name);
            var number = FindViewById<TextView>(Resource.Id.rv_number);
            var photo = FindViewById<ImageView>(Resource.Id.rv_photo);

            name.Text = contact.Name;
            number.Text = contact.Number;
            photo.SetImageResource(contact.Img);

            var Card = FindViewById<CardView>(Resource.Id.rv_card);
            Card.Radius = 72;
        }
    }
}