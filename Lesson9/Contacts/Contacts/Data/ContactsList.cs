﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Contacts
{
    class ContactsList
    {
        public List<Contact> Contacts { get; set; }

        public ContactsList()
        {
            FillList();
        }

        private void FillList()
        {
            Contacts = new List<Contact>
            {
                new Contact(Resource.Drawable.Yoda_min, "Master Yoda", "+380633213210"),
                new Contact(Resource.Drawable.ObiWan_min, "Obi-Wan Kenobi", "+380633213210"),
                new Contact(Resource.Drawable.QuiGon_min, "Qui-Gon Jinn", "+380633213210"),
                new Contact(Resource.Drawable.MaceWindu_min, "Mace Windu", "+380633213210"),
                new Contact(Resource.Drawable.Luke_min, "Luke Skywalker", "+380633213210"),
                new Contact(Resource.Drawable.KiAdi_min, "Ki Adi Mundi", "+380633213210"),
                new Contact(Resource.Drawable.Maul_min, "Darth Maul", "+380633213210"),
                new Contact(Resource.Drawable.Dooku_min, "Count Dooku", "+380633213210"),
                new Contact(Resource.Drawable.Palpatine_min, "Emperor Palpatine", "+380633213210"),
                new Contact(Resource.Drawable.Vader_min, "Darth Vader", "+380633213210"),
                new Contact(Resource.Drawable.Krennik_min, "Orson Krennik", "+380633213210"),
                new Contact(Resource.Drawable.Tarkin_min, "Grand Moff Tarkin", "+380633213210"),
                new Contact(Resource.Drawable.Leia_min, "Leia Organa", "+380633213210"),
                new Contact(Resource.Drawable.Solo_min, "Han Solo", "+380633213210"),
                new Contact(Resource.Drawable.Chewbacca_min, "Chewbacca", "+380633213210"),
                new Contact(Resource.Drawable.JarJar_min, "Jar Jar Binks", "+380633213210"),
                new Contact(Resource.Drawable.Jabba_min, "Jabba Hutt", "+380633213210"),
                new Contact(Resource.Drawable.Rey_min, "Rey", "+380633213210"),
                new Contact(Resource.Drawable.Finn_min, "Finn", "+380633213210"),
                new Contact(Resource.Drawable.Poe_min, "Poe Dameron", "+380633213210"),
                new Contact(Resource.Drawable.Hux_min, "General Hux", "+380633213210"),
                new Contact(Resource.Drawable.KyloRen_min, "Kylo Ren", "+380633213210"),
                new Contact(Resource.Drawable.Phasma_min, "Captain Phasma", "+380633213210"),
                new Contact(Resource.Drawable.DJ_min, "DJ", "+380633213210"),
                new Contact(Resource.Drawable.Snoke_min, "Supreme Leader Snoke", "+380633213210")
                
            };
        }

    }
}