﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Contacts
{
    class Contact
    {
        private int _img;
        private string _name;
        private string _number;

        public int Img { get => _img; set => _img = value; }
        public string Name { get => _name; set => _name = value; }
        public string Number { get => _number; set => _number = value; }
        
        public Contact(int img, string name, string number)
        {
            _img = img;
            _name = name;
            _number = number;
        }
    }
}