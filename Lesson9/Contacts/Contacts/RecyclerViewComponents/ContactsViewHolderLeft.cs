﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Contacts.Views;
using Android.Widget;

namespace Contacts
{
    class ContactsViewHolderLeft : RecyclerView.ViewHolder, ContactsViewHolder
    {
        LeftView leftView;
        public ContactsViewHolderLeft(View itemView) : base (itemView)
        {
            leftView = itemView.FindViewById<LeftView>(Resource.Id.hl_view);
        }

        public void SetData(Contact contact)
        {
            leftView.SetData(contact);
        }
    }
}