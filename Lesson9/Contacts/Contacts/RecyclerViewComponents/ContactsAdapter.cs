﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace Contacts
{
    class ContactsAdapter : RecyclerView.Adapter
    {
        public override int ItemCount
        {
            get
            {
                return Contacts.Count+1;
            }
        }

        public override int GetItemViewType(int position)
        {
            if (position == ItemCount - 1 )
            {
                return 2;
            }
            else if (position%2 == 0)
            {
                return 0;
            }
            else if (position % 2 == 1)
            {
                return 1;
            }

            return -10;
        }

        public List<Contact> Contacts { get; set; }

        public ContactsAdapter(List<Contact> contacts)
        {
            Contacts = contacts;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is SpinnerHolder)
            {
                SpinnerHolder spinnerHolder = holder as SpinnerHolder;
                return;
            }

            ContactsViewHolder contactsViewHolder;
            if (holder is ContactsViewHolderLeft)
            {
                contactsViewHolder = holder as ContactsViewHolderLeft;
                
            }
            else if (holder is ContactsViewHolderRight)
            {
                contactsViewHolder = holder as ContactsViewHolderRight;
            }
            else
            {
                throw new ArgumentException("wrong holder type");
            }
            contactsViewHolder.SetData(Contacts[position]);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView;
            if (viewType == 0)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_left, parent, false);
                ContactsViewHolderLeft contactsViewHolder = new ContactsViewHolderLeft(itemView);
                return contactsViewHolder;
            }
            else if (viewType == 1)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_right, parent, false);
                ContactsViewHolderRight contactsViewHolder = new ContactsViewHolderRight(itemView);
                return contactsViewHolder;
            }
            else if (viewType == 2)
            {
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.h_spinner, parent, false);
                SpinnerHolder contactsViewHolder = new SpinnerHolder(itemView);
                return contactsViewHolder;
            }
            else
            {
                throw new ArgumentException("wrong viewType");
            }
        }
    }
}