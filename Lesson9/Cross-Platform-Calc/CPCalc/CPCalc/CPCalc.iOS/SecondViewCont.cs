using Foundation;
using System;
using UIKit;
using Portable;

namespace CPCalc.iOS
{
    public partial class SecondViewCont : UIViewController
    {
        public SecondViewCont (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
            base.ViewDidLoad();
		}

		public override void ViewWillAppear(bool animated)
		{
            base.ViewWillAppear(animated);
		}

		public override void ViewDidAppear(bool animated)
		{
            base.ViewDidAppear(animated);

            if (Calculation.Result != null) {
                edText.Text = Calculation.Result;
            } else {
                edText.Text = "There are any values";
            }
		}

		partial void BtnGoBack_TouchUpInside(UIButton sender)
        {
            Calculation.Result = edText.Text;
            DismissViewController(false, null);
            NavigationRouter.GoToFirstView(this, true);
        }
    }
}