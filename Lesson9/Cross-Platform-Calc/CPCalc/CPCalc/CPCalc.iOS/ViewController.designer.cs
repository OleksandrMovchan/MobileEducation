// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace CPCalc.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnCancel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnDivide { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnEight { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnEquals { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnFive { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnFour { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnMinus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnMultiple { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnNine { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnOne { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnPlus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSeven { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSix { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnThree { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnTwo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnZero { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel txtResult { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnCancel != null) {
                btnCancel.Dispose ();
                btnCancel = null;
            }

            if (btnDivide != null) {
                btnDivide.Dispose ();
                btnDivide = null;
            }

            if (btnEight != null) {
                btnEight.Dispose ();
                btnEight = null;
            }

            if (btnEquals != null) {
                btnEquals.Dispose ();
                btnEquals = null;
            }

            if (btnFive != null) {
                btnFive.Dispose ();
                btnFive = null;
            }

            if (btnFour != null) {
                btnFour.Dispose ();
                btnFour = null;
            }

            if (btnMinus != null) {
                btnMinus.Dispose ();
                btnMinus = null;
            }

            if (btnMultiple != null) {
                btnMultiple.Dispose ();
                btnMultiple = null;
            }

            if (btnNine != null) {
                btnNine.Dispose ();
                btnNine = null;
            }

            if (btnOne != null) {
                btnOne.Dispose ();
                btnOne = null;
            }

            if (btnPlus != null) {
                btnPlus.Dispose ();
                btnPlus = null;
            }

            if (btnSeven != null) {
                btnSeven.Dispose ();
                btnSeven = null;
            }

            if (btnSix != null) {
                btnSix.Dispose ();
                btnSix = null;
            }

            if (btnThree != null) {
                btnThree.Dispose ();
                btnThree = null;
            }

            if (btnTwo != null) {
                btnTwo.Dispose ();
                btnTwo = null;
            }

            if (btnZero != null) {
                btnZero.Dispose ();
                btnZero = null;
            }

            if (txtResult != null) {
                txtResult.Dispose ();
                txtResult = null;
            }
        }
    }
}