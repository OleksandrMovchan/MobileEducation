// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CPCalc.iOS
{
    [Register ("SecondViewCont")]
    partial class SecondViewCont
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnGoBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField edText { get; set; }

        [Action ("BtnGoBack_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BtnGoBack_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnGoBack != null) {
                btnGoBack.Dispose ();
                btnGoBack = null;
            }

            if (edText != null) {
                edText.Dispose ();
                edText = null;
            }
        }
    }
}