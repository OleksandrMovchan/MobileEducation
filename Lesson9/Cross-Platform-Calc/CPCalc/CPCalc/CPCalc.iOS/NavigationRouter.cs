﻿using System;
using Foundation;
using UIKit;
namespace CPCalc.iOS
{
    public static class NavigationRouter
    {
        public static void GoToSecondView(UIViewController controller, bool animated) 
        {
            var destination = UIStoryboard.FromName("Main", NSBundle.MainBundle).InstantiateViewController("SecondViewCont") as SecondViewCont;

            if (destination != null)
            {
                //controller.ModalPresentationStyle = UIModalPresentationStyle.Custom;
                TransitioningDelegate transitioningDelegate = new TransitioningDelegate();
                controller.TransitioningDelegate = transitioningDelegate;
                controller.PresentViewController(destination, true, null);
            }

        }

        public static void GoToFirstView(UIViewController controller, bool animated)
        {
            var destination = UIStoryboard.FromName("Main", NSBundle.MainBundle).InstantiateViewController("ViewController") as ViewController;
            if (destination != null)
            {
                //controller.ModalPresentationStyle = UIModalPresentationStyle.Custom;
                TransitioningDelegate transitioningDelegate = new TransitioningDelegate();
                controller.TransitioningDelegate = transitioningDelegate;
                controller.PresentViewController(destination, true, null);
            }

        }
    }
}
