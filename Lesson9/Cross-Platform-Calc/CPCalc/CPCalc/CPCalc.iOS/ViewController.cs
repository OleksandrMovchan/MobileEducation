﻿using System;
using Foundation;
using UIKit;

using Portable;

namespace CPCalc.iOS
{
    public partial class ViewController : UIViewController
    {
        double firstNum = 0;
        double secondNum = 0;
        double result = 0;
        string operation = "+";
        bool calculated = false;

        public string resultValue { get; set; }

        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            if (resultValue == null) resultValue = "0";

        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);


            btnOne.TouchUpInside -= NumberPressed;
            btnTwo.TouchUpInside -= NumberPressed;
            btnThree.TouchUpInside -= NumberPressed;
            btnFour.TouchUpInside -= NumberPressed;
            btnFive.TouchUpInside -= NumberPressed;
            btnSix.TouchUpInside -= NumberPressed;
            btnSeven.TouchUpInside -= NumberPressed;
            btnEight.TouchUpInside -= NumberPressed;
            btnNine.TouchUpInside -= NumberPressed;
            btnZero.TouchUpInside -= NumberPressed;

            btnPlus.TouchUpInside -= OperationPressed;
            btnMinus.TouchUpInside -= OperationPressed;
            btnMultiple.TouchUpInside -= OperationPressed;
            btnDivide.TouchUpInside -= OperationPressed;

            btnCancel.TouchUpInside -= CancelPressed;

            btnEquals.TouchUpInside -= EqualsPressed;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            txtResult.Text = resultValue;

            btnOne.TouchUpInside += NumberPressed;
            btnTwo.TouchUpInside += NumberPressed;
            btnThree.TouchUpInside += NumberPressed;
            btnFour.TouchUpInside += NumberPressed;
            btnFive.TouchUpInside += NumberPressed;
            btnSix.TouchUpInside += NumberPressed;
            btnSeven.TouchUpInside += NumberPressed;
            btnEight.TouchUpInside += NumberPressed;
            btnNine.TouchUpInside += NumberPressed;
            btnZero.TouchUpInside += NumberPressed;

            btnPlus.TouchUpInside += OperationPressed;
            btnMinus.TouchUpInside += OperationPressed;
            btnMultiple.TouchUpInside += OperationPressed;
            btnDivide.TouchUpInside += OperationPressed;

            btnCancel.TouchUpInside += CancelPressed;

            btnEquals.TouchUpInside += EqualsPressed;


            if (Calculation.Result != null)
            {
                txtResult.Text = Calculation.Result;
            }
            else
            {
                txtResult.Text = "0";
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            //Release any cached data, images, etc that aren't in use.
        }

        private void NumberPressed(object sender, EventArgs eventArgs)
        {
            string number = ((UIButton)sender).Title(UIControlState.Normal);
            Calculation.NumberPressed(number);
            txtResult.Text = Calculation.Result;
        }

        private void OperationPressed(object sender, EventArgs eventArgs)
        {
            string operation = ((UIButton)sender).Title(UIControlState.Normal);
            Calculation.OperationPressed(operation);
            txtResult.Text = Calculation.Result;
        }

        private void CancelPressed(object sender, EventArgs eventArgs)
        {
            Calculation.CancelPressed();
            txtResult.Text = Calculation.Result;
        }

        private void EqualsPressed(object sender, EventArgs eventArgs)
        {
            Calculation.EqualsPressed();
            txtResult.Text = Calculation.Result;

            DismissViewController(false, null);
            NavigationRouter.GoToSecondView(this, true);
        }
    }
}
