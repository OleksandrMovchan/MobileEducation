﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;

using Portable;

namespace CPCalc.Droid
{
    [Activity(Label = "CPCalc.Droid", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity, NextView
    {
        TextView txtResult;
        /*
        double numberOne = 0;
        double numberTwo = 0;
        string operation = "+";
        bool isCalculated = false;
        */

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
        }

        protected override void OnStart()
        {
            base.OnStart();

            var btnOne = FindViewById<Button>(Resource.Id.btnOne);
            var btnTwo = FindViewById<Button>(Resource.Id.btnTwo);
            var btnThree = FindViewById<Button>(Resource.Id.btnThree);
            var btnFour = FindViewById<Button>(Resource.Id.btnFour);
            var btnFive = FindViewById<Button>(Resource.Id.btnFive);
            var btnSix = FindViewById<Button>(Resource.Id.btnSix);
            var btnSeven = FindViewById<Button>(Resource.Id.btnSeven);
            var btnEight = FindViewById<Button>(Resource.Id.btnEight);
            var btnNine = FindViewById<Button>(Resource.Id.btnNine);
            var btnZero = FindViewById<Button>(Resource.Id.btnZero);

            var btnPlus = FindViewById<Button>(Resource.Id.btnPlus);
            var btnMinus = FindViewById<Button>(Resource.Id.btnMinus);
            var btnMultiple = FindViewById<Button>(Resource.Id.btnMultiple);
            var btnDivide = FindViewById<Button>(Resource.Id.btnDivide);
            var btnEquals = FindViewById<Button>(Resource.Id.btnEquals);

            var btnCancel = FindViewById<Button>(Resource.Id.btnCancel);

            txtResult = FindViewById<TextView>(Resource.Id.txtResult);

            btnOne.Click += NumberPressed;
            btnTwo.Click += NumberPressed;
            btnThree.Click += NumberPressed;
            btnFour.Click += NumberPressed;
            btnFive.Click += NumberPressed;
            btnSix.Click += NumberPressed;
            btnSeven.Click += NumberPressed;
            btnEight.Click += NumberPressed;
            btnNine.Click += NumberPressed;
            btnZero.Click += NumberPressed;

            btnPlus.Click += OperationPressed;
            btnMinus.Click += OperationPressed;
            btnMultiple.Click += OperationPressed;
            btnDivide.Click += OperationPressed;

            btnCancel.Click += CancelPressed;

            btnEquals.Click += EqualsPressed;

            if (Calculation.Result != null)
            {
                txtResult.Text = Calculation.Result;
            }
            else
            {
                txtResult.Text = "0";
            }

        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        private void NumberPressed(object sender, EventArgs e)
        {
            string number = ((Button)sender).Text;
            Calculation.NumberPressed(number);
            txtResult.Text = Calculation.Result;
        }
        
        private void OperationPressed(object sender, EventArgs e)
        {
            string operation = ((Button)sender).Text;
            Calculation.OperationPressed(operation);
            txtResult.Text = Calculation.Result;
        }

        private void CancelPressed(object sender, EventArgs e)
        {
            Calculation.CancelPressed();
            txtResult.Text = Calculation.Result;
        }

        private void EqualsPressed(object sender, EventArgs e)
        {
            Calculation.EqualsPressed();
            txtResult.Text = Calculation.Result;
            ToNextView();
        }

        public void ToNextView()
        {
            Intent intent = new Intent(this, typeof(SecondActivity));
            StartActivity(intent);
        }
    }
}

