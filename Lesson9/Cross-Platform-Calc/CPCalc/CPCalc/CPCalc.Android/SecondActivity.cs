﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Portable;

namespace CPCalc.Droid
{
    class SecondActivity : Activity
    {
        EditText result;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SecondActivity);
        }

        protected override void OnStart()
        {
            base.OnStart();

            var btnBack = FindViewById<Button>(Resource.Id.btnBack);
            btnBack.Click += GoBack;

            result = FindViewById<EditText>(Resource.Id.edt_result);
            result.Text = Calculation.Result;

        }

        private void GoBack (object sender, EventArgs e)
        {
            Calculation.Result = result.Text;
            Finish();
        }

    }
}