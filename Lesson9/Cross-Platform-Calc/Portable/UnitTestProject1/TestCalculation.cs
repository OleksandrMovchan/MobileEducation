﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Portable;

namespace UnitTestProject1
{
    [TestClass]
    public class TestCalculation
    {
       
        [TestMethod]
        public void Test_CancelPressed_Num1()
        {
            Calculation.CancelPressed();
            Assert.AreEqual(0, Calculation.Num1, "Num1 is not zero!");
        }

        [TestMethod]
        public void Test_CancelPressed_Num2()
        {
            Calculation.CancelPressed();
            Assert.AreEqual(0, Calculation.Num2, "Num2 is not zero!");
        }

        [TestMethod]
        public void Test_CancelPressed_Result()
        {
            Calculation.CancelPressed();
            Assert.AreEqual("0", Calculation.Result, "Result is not zero!");
        }

        [TestMethod]
        public void Test_NumberPressed_OneNumber()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Assert.AreEqual("4", Calculation.Result, "Number was added incorrectly");
        }

        [TestMethod]
        public void Test_NumberPressed_SomeNumbers()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Assert.AreEqual("48", Calculation.Result, "Number was added incorrectly");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Argument is not a digit")]
        public void Test_NumberPressed_NullArgument()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Argument is not a digit")]
        public void Test_NumberPressed_EmptyString()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Argument is not a digit")]
        public void Test_NumberPressed_NotANumber()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("qw");
        }

        //валидные и не валидные значения, пустая строка, null|
        [TestMethod]
        public void Test_OperationPressed_SentPlus()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("+");
            Assert.AreEqual(48, Calculation.Num1, "Num1 was written incorrectly");
            Assert.AreEqual("+", Calculation.Operation, "Operation was written incorrectly");
        }

        [TestMethod]
        public void Test_OperationPressed_SentMinus()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("-");
            Assert.AreEqual(48, Calculation.Num1, "Num1 was written incorrectly");
            Assert.AreEqual("-", Calculation.Operation, "Operation was written incorrectly");
        }
        
        [TestMethod]
        public void Test_OperationPressed_SentMult()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("*");
            Assert.AreEqual(48, Calculation.Num1, "Num1 was written incorrectly");
            Assert.AreEqual("*", Calculation.Operation, "Operation was written incorrectly");
        }

        [TestMethod]
        public void Test_OperationPressed_SentDiv()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("/");
            Assert.AreEqual(48, Calculation.Num1, "Num1 was written incorrectly");
            Assert.AreEqual("/", Calculation.Operation, "Operation was written incorrectly");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid operation")]
        public void Test_OperationPressed_SentNull()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid operation")]
        public void Test_OperationPressed_SentInvalid()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("f");
        }

        [TestMethod]
        public void Test_EqualsPressed_NoValues()
        {
            Calculation.CancelPressed();
            Calculation.EqualsPressed();
            Assert.AreEqual("0", Calculation.Result, "First result is not zero");
        }

        [TestMethod]
        public void Test_EqualsPressed_Addiction()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("+");
            Calculation.NumberPressed("5");
            Calculation.EqualsPressed();
            Assert.AreEqual("53", Calculation.Result, "Addiction works incorrectly");
        }

        [TestMethod]
        public void Test_EqualsPressed_Subtraction()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.NumberPressed("8");
            Calculation.OperationPressed("-");
            Calculation.NumberPressed("5");
            Calculation.EqualsPressed();
            Assert.AreEqual("43", Calculation.Result, "Subtraction works incorrectly");
        }

        [TestMethod]
        public void Test_EqualsPressed_Multiplication()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("4");
            Calculation.OperationPressed("*");
            Calculation.NumberPressed("5");
            Calculation.EqualsPressed();
            Assert.AreEqual("20", Calculation.Result, "Multiplication works incorrectly");
        }

        [TestMethod]
        public void Test_EqualsPressed_Division()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("20");
            Calculation.OperationPressed("/");
            Calculation.NumberPressed("5");
            Calculation.EqualsPressed();
            Assert.AreEqual("4", Calculation.Result, "Division works incorrectly");
        }

        [TestMethod]
        public void Test_EqualsPressed_DivisionByZero()
        {
            Calculation.CancelPressed();
            Calculation.NumberPressed("20");
            Calculation.OperationPressed("/");
            Calculation.NumberPressed("0");
            Calculation.EqualsPressed();
            Assert.AreEqual("Infinity", Calculation.Result, "Division by zero works incorrectly");
        }
    }
}
