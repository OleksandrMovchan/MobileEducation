﻿using System;

namespace Portable
{
    public class Calculation
    {
        private static double _num1 = 0;
        private static double _num2 = 0;
        private static string _operation = "+";
        private static string _result = "0";
        private static bool _calculated = false;

        public static double Num1 { get => _num1; set => _num1 = value; }
        public static double Num2 { get => _num2; set => _num2 = value; }
        public static string Operation { get => _operation; set => _operation = value; }
        public static string Result { get => _result; set => _result = value; }
        public static bool Calculated { get => _calculated; set => _calculated = value; }

        public static void NumberPressed(string num)
        {
            try
            {
                int.Parse(num);
            }
            catch 
            {
                throw new ArgumentException("Argument is not a digit");
            }
            
            if (Calculated)
            {
                Result = "0";
                AddNumber(num);
                Calculated = false;
            }
            else
            {
                AddNumber(num);
            }
        }

        private static void AddNumber(string number)
        {
            if (Result.Equals("0") || Result.Equals("Infinity") || Result.Equals("Not an operation"))
            {
                Result = number;
            }
            else
            {
                Result += number;
            }
        }

        public static void OperationPressed(string oper)
        {
            if (oper != "+" && oper != "-" && oper != "*" && oper != "/")
            {
                throw new ArgumentException("Invalid operation");
            }
            Operation = oper;
            string valueNumberOne = Result;
            Num1 = double.Parse(valueNumberOne);
            Result = "0";
        }

        public static void CancelPressed()
        {
            Num1 = 0;
            Num2 = 0;
            Result = "0";
            Operation = "+";
        }

        public static void EqualsPressed()
        {
            string valueNumberTwo = Result;
            Num2 = double.Parse(valueNumberTwo);
            Result = "";
            switch (Operation)
            {
                case "+":
                    Result += string.Concat(Num1 + Num2);
                    break;
                case "-":
                    Result += string.Concat(Num1 - Num2);
                    break;
                case "*":
                    Result += string.Concat(Num1 * Num2);
                    break;
                case "/":
                    if (Num2 != 0)
                    {
                        Result += string.Concat(Num1 / Num2);
                    }
                    else
                    {
                        Result = "Infinity";
                    }
                    break;
                default:
                    Result = "Not an operation";
                    break;
            }

            Num1 = 0;
            Num2 = 0;
        }

    }
}
